<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_home_model extends CI_Model
{
    var $table = 'tbl_home';

    function getInfo()
    {
        $this->db->where('id',1);
        $query = $this->db->get($this->table,1);
        return $query->result();
    }

    function editPost($info)
    {
        $this->db->where('id', 1);
        $this->db->update($this->table, $info);
        
        return TRUE;
    }
    


}

  