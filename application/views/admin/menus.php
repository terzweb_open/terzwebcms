<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-sort-amount-desc"></i> メニュー管理
        <small>確認・追加・編集・削除</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url($this->adminurl); ?>/menus/addNew"><i class="fa fa-plus"></i> 追加</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?php
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">メニューリスト</h3>
                        <div class="box-tools">                                
                             
                        </div>
                    </div><!-- /.box-header -->


                <div class="box-body table-responsive">


                
                    <table class="table table-striped table-bordered table-result" id="example1">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>タイトル</th>
                                <th>リンクURL</th>
                                <th>表示順</th>
                                <th>ステータス</th>
                            <th class="tabledit-toolbar-column"></th></tr>
                        </thead>
                        <tbody>
                            
                            <?php
                    
                    if(!empty($pageRecords))
                    {
                      foreach($pageRecords as $record)
                            {
                        ?>
                            <tr id="<?php echo $record->id ?>">
                                <td><span class="tabledit-span tabledit-identifier"><?php echo $record->id ?></span><input class="tabledit-input tabledit-identifier" type="hidden" name="id" value="<?php echo $record->id ?>" disabled=""></td>
                                <td class="tabledit-view-mode"><span class="tabledit-span"><?php echo $record->value ?></span><input class="tabledit-input form-control input-sm" type="text" name="value" value="<?php echo $record->value ?>" style="display: none;" disabled=""></td>
                                <td class="tabledit-view-mode"><span class="tabledit-span"><?php echo $record->url ?></span><input class="tabledit-input form-control input-sm" type="text" name="url" value="<?php echo $record->url ?>" style="display: none;" disabled=""></td>
                                <td class="tabledit-view-mode"><span class="tabledit-span"><?php echo $record->sort ?></span><input class="tabledit-input form-control input-sm" type="number" name="sort" value="<?php echo $record->sort ?>" style="display: none;" disabled=""></td>
                                <td class="tabledit-view-mode"><span class="tabledit-span"><?php echo $menu_status[$record->status] ?></span><input class="tabledit-input form-control input-sm" type="text" name="status" value="<?php echo $menu_status[$record->status] ?>" style="display: none;" disabled=""></td>
                            
                            </tr>
                            <?php
                            }
                    }
                    ?>
                        </tbody>
                    </table>
                    
                </div>
            
        </div>

    </div>
    </section>
</div>


<script>
$(function () {

    $('#example1').Tabledit({
        url: '<?php echo base_url($this->adminurl)?>/menus/editupdate',
        columns: {
            identifier: [0, 'id'],
            editable:[ [1, 'value'],[2,'url'],[3,'sort'],[4,'status', '{"0": "非表示", "1": "表示"}'] ]
        }
    });

});
</script>
