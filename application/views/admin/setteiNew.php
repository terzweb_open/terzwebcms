<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-gear"></i> 設定
        <small>Add config</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->

              <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                

                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">設定追加</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <form role="form" id="addForm" action="<?php echo base_url($this->adminurl) ?>/setting/adding" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname"><span class="text-danger">※</span>configname</label>
                                        <input type="text" class="form-control required" value="<?php echo set_value('configname','');?>" name="configname">
                                    </div>
                                    
                                </div>

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">value</label>
                                        <input type="text" class="form-control" value="" name="value">
                                    </div>
                                    
                                </div>
                            </div>
                            
                            
                        </div><!-- /.box-body -->    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="設定追加" />
                            <input type="reset" class="btn btn-default" value="リセット" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-12">
                
            </div>
        </div>    
    </section>
    
</div>








<script type="text/javascript">
    var baseURL = "<?php echo base_url($this->adminurl); ?>";
</script>


<script type="text/javascript">
$().ready(function(){
    
    //独自の検証ルールを設定
    var methods = {
        //電話番号
        alphanumeric: function(value, element){
            return this.optional(element) || /^[a-zA-Z0-9!-/:-@¥[-`{-~]*$/.test(value);
        },
        //半角英数字
        alphanum: function(value, element){
            return this.optional(element) || /^([a-zA-Z0-9_]+)$/.test(value);
            }

    };
    
	var addThisForm = $("#addForm");
	
	var validator = addThisForm.validate({
		
		rules:{
            configname : { required : true, alphanum:true }
                    },
		messages:{
            configname : { required : "入力必須項目です", alphanum:"半角英字" }
		}
	});
    $.each(methods, function(key) {
        $.validator.addMethod(key, this);
    });
    jQuery.validator.addMethod('selectcheck', function (value) {
        return (value != '0');
    });


});
</script>