<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Post extends MY_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('posts_model');
        $this->load->model('config_model');
  
    }

    public function index()
    {
        $this->page();
    }
    
    public function page() {
        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');        
        //記事
        
        $config['total_rows'] = $this->posts_model->ListingCount($searchText); // 件数
        
        $config['per_page'] = $this->config_model->getconfig('list_blog_count');
        $config['uri_segment'] = 3;
        $returns = $this->paginationCompress ( "post/page/", $config['total_rows'], $config['per_page'], $config['uri_segment'] );

        $data1 = $this->posts_model->Listing($searchText, $returns["page"], $returns["segment"]);
        


        $this->data = array(
                    'page_title'   => 'ブログリスト',
                    'blog_active' => 'active"',
                    'blog_entries' => $data1,
                    'pagenation' => $this->pagination->create_links()
            );
        $this->make_temp('blog_list_view', 'blog');
    }


    public function id() {

        if (empty($this->uri->segment(2))) {
            show_404($page = '', $log_error = TRUE);
        }

        //テーブルpagesのtitleと参照
        $data['blogdata'] = $this->posts_model->find_by_url($this->uri->segment(2));
        
        //件数無しだったらページノットファウンド
        if (empty($data['blogdata'])) {
            show_404($page = '', $log_error = TRUE);
        }
        


        $this->data = $data['blogdata'];
        
        $this->make_temp('blog_view', 'blog');

    }


    
    
}