<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_news_model extends CI_Model
{
    var $table = 'tbl_news';
    /**
     * ニュースカウント
     */
    function newsListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.status, BaseTbl.id, BaseTbl.title, BaseTbl.contents, BaseTbl.status, BaseTbl.opendate, BaseTbl.postdata');
        $this->db->from('tbl_news as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.title  LIKE '%".$searchText."%'
                            OR  BaseTbl.contents  LIKE '%".$searchText."%'
                            OR  BaseTbl.contents_sub  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted !=', 3);
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    /**
     * ニュース表示
     */
    function newsListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.status, BaseTbl.id, BaseTbl.title, BaseTbl.contents, BaseTbl.opendate, BaseTbl.postdata');
        $this->db->from('tbl_news as BaseTbl');

        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.title  LIKE '%".$searchText."%'
                            OR  BaseTbl.contents  LIKE '%".$searchText."%'
                            OR  BaseTbl.contents_sub  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted !=', 3);
        $this->db->limit($page, $segment);
        $this->db->order_by('opendate','desc');
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
 
    
    /**
     * 新規
     */
    function addNewNews($userInfo)
    {
        $this->db->trans_start();
        $this->db->insert($this->table, $userInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     *　対応IDを表示
     */
    function getNewsInfo($id)
    {
        $this->db->where('id',$id);
        $query = $this->db->get($this->table,1);
        return $query->result();
    }
    
    
    /**
     * 更新処理
     */
    function editNews($userInfo, $userId)
    {
        $this->db->where('id', $userId);
        $this->db->update($this->table, $userInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * 削除
     */
    function deleteNews($id, $datainfo)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_news', $datainfo);
        
        return $this->db->affected_rows();
    }

}

  