<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


/**
 * This function is used to print the content of any data
 */
function pre($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

/**
 * This function used to get the CI instance
 */
if(!function_exists('get_instance'))
{
    function get_instance()
    {
        $CI = &get_instance();
    }
}

/**
 * This function used to generate the hashed password
 * @param {string} $plainPassword : This is plain text password
 */
if(!function_exists('getHashedPassword'))
{
    function getHashedPassword($plainPassword)
    {
        return password_hash($plainPassword, PASSWORD_DEFAULT);
    }
}

/**
 * This function used to generate the hashed password
 * @param {string} $plainPassword : This is plain text password
 * @param {string} $hashedPassword : This is hashed password
 */
if(!function_exists('verifyHashedPassword'))
{
    function verifyHashedPassword($plainPassword, $hashedPassword)
    {
        return password_verify($plainPassword, $hashedPassword) ? true : false;
    }
}

/**
 * This method used to get current browser agent
 */
if(!function_exists('getBrowserAgent'))
{
    function getBrowserAgent()
    {
        $CI = get_instance();
        $CI->load->library('user_agent');

        $agent = '';

        if ($CI->agent->is_browser())
        {
            $agent = $CI->agent->browser().' '.$CI->agent->version();
        }
        else if ($CI->agent->is_robot())
        {
            $agent = $CI->agent->robot();
        }
        else if ($CI->agent->is_mobile())
        {
            $agent = $CI->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }

        return $agent;
    }
}

if(!function_exists('setProtocol'))
{
    function setProtocol()
    {
        $CI = &get_instance();
                    
        $CI->load->library('email');
        
        $config['protocol'] = PROTOCOL;
        $config['mailpath'] = MAIL_PATH;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['smtp_timeout'] = '20';
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        $config['smtp_crypto'] = 'ssl';
        $config['priority'] = '4';
        $config['wrapchars'] = "250000";
        $config['wordwrap'] = FALSE;
        
        
        $CI->email->initialize($config);
        
        return $CI;
    }
}

if(!function_exists('emailConfig'))
{
    function emailConfig()
    {
        $CI->load->library('email');
        $config['protocol'] = PROTOCOL;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['mailpath'] = MAIL_PATH;
        $config['charset'] = 'UTF-8';
        $config['mailtype'] = "text";
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = FALSE;
        $config['smtp_crypto'] = 'ssl';
    }
}
// 受付時のメール送信
if(!function_exists('completeUketsukeEmail'))
{
    function completeUketsukeEmail($detail)
    {
        $data["data"] = $detail;
        // pre($detail);
        // die;
        
        $CI = setProtocol();        
        
        $CI->email->from(EMAIL_FROM, FROM_NAME);
        $CI->email->subject("遺品整理コンシェルジュ 無料相談を承りました");
        $CI->email->message($CI->load->view('email/completeUketsukeEmail', $data, TRUE));
        $CI->email->to($detail["email"]);
        $CI->email->bcc($detail["bccemail"]);
        $status = $CI->email->send();
        
        return $status;
    }
}
// お問い合わせのメール送信
if(!function_exists('completeContactEmail'))
{
    function completeContactEmail($detail)
    {
        $data["data"] = $detail;
        // pre($detail);
        // die;
        
        $CI = setProtocol();        
        
        $CI->email->from(EMAIL_FROM, FROM_NAME);
        $CI->email->subject("terreto.workへのお問い合わせを承りました");
        $CI->email->message($CI->load->view('email/completeContactEmail', $data, TRUE));
        $CI->email->to($detail["email"]);
        $CI->email->bcc($detail["bccemail"]);
        $status = $CI->email->send();
        
        return $status;
    }
}

if(!function_exists('resetPasswordEmail'))
{
    function resetPasswordEmail($detail)
    {
        $data["data"] = $detail;
        // pre($detail);
        // die;
        
        $CI = setProtocol();        
        
        $CI->email->from(EMAIL_FROM, FROM_NAME);
        $CI->email->subject("パスワードリセットのご案内");
        $CI->email->message($CI->load->view('email/resetPassword', $data, TRUE));
        $CI->email->to($detail["email"]);
        $status = $CI->email->send();
        
        return $status;
    }
}

if(!function_exists('entryformEmail'))
{
    function entryformEmail($detail)
    {
        $data["data"] = $detail;
        // pre($detail);
        // die;
        
        $CI = setProtocol();        
        
        $CI->email->from(EMAIL_FROM, FROM_NAME);
        $CI->email->subject("仮登録のご案内");
        $CI->email->message($CI->load->view('email/entryformEmail', $data, TRUE));
        $CI->email->to($detail["email"]);
        $status = $CI->email->send();
        
        return $status;
    }
}
if(!function_exists('completeformEmail'))
{
    function completeformEmail($detail)
    {
        $data["data"] = $detail;
        // pre($detail);
        // die;
        
        $CI = setProtocol();        
        
        $CI->email->from(EMAIL_FROM, FROM_NAME);
        $CI->email->subject("登録完了のご案内");
        $CI->email->message($CI->load->view('email/completeformEmail', $data, TRUE));
        $CI->email->to($detail["email"]);
        $status = $CI->email->send();
        
        return $status;
    }
}
if(!function_exists('completebuyingorderEmail'))
{
    function completebuyingorderEmail($detail)
    {
        $data["data"] = $detail;
        // pre($detail);
        // die;
        
        $CI = setProtocol();        
        
        $CI->email->from(EMAIL_FROM, FROM_NAME);
        $CI->email->subject("ご注文ありがとうございます");
        $CI->email->message($CI->load->view('email/completeOrderEmail', $data, TRUE));
        $CI->email->to($detail["email"]);
        $status = $CI->email->send();
        
        return $status;
    }
}

if(!function_exists('personalsendMailMagazine'))
{
    function personalsendMailMagazine($detail)
    {
        $data["data"] = $detail;
        
        
        $CI = setProtocol();        
        //pre($CI);
        //die;
        $CI->email->from(EMAIL_FROM, FROM_NAME);
        $CI->email->subject($detail["magazine_title"]);
        #$CI->email->message($data["data"]["magazine_contents"]);
        $CI->email->message($CI->load->view('email/personalsendmagazine', $data, TRUE));
        $CI->email->to($detail["email"]);
        $status = $CI->email->send();
        
        return $status;
    }
}

if(!function_exists('setuploadprotocol'))
{
    function setuploadprotocol()
    {
        $config['upload_path'] = UPLOAD_PATH;
        $config['allowed_types'] = ALLOWD_TYPES;
        $config['max_size'] = MAX_SIZE;
        $config['max_width'] = MAX_WIDTH;
        $config['max_height'] = MAX_HEIGHT;
        $config['encrypt_name'] = TRUE;

        return $config;
    }
}


if(!function_exists('setFlashData'))
{
    function setFlashData($status, $flashMsg)
    {
        $CI = get_instance();
        $CI->session->set_flashdata($status, $flashMsg);
    }
}



if(!function_exists('nengetu_format'))
{
    function nengetu_format($str){

        $year = substr($str,0,4);
        $month = substr($str,4,2);

        $setyn = $year . '年'. $month . '月';

        return $setyn;
    }
}

if(!function_exists('nengetu_henkan'))
{
    function nengetu_henkan($str){

        $year = substr($str,0,4);
        $month = substr($str,5,2);
        $day = substr($str,8,2);

        $setyn = $year . '/'. $month . '/'.$day;

        return $setyn;
    }
}



if(!function_exists('time_format'))
{
    function time_format($str)
    {
       $ft = sprintf('%06d',$str);
       $time = substr($ft, 0, -2);
       $array = str_split($time, 2);
       $time = $array[0] .' : '.  $array[1]; 

       return $time;

    }

}



?>