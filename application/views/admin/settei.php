<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-gear"></i> 設定
        <small>確認・追加・編集・削除</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url($this->adminurl); ?>/setting/addNew"><i class="fa fa-plus"></i> 設定値追加</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?php
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">設定リスト</h3>
                        <div class="box-tools">                                
                             
                        </div>
                    </div><!-- /.box-header -->


                <div class="box-body table-responsive">
                    <table class="table table-striped table-bordered table-result" id="example1">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ConfigName(編集不可）</th>
                                <th>Value</th>
                            <th class="tabledit-toolbar-column"></th></tr>
                        </thead>
                        <tbody>
                            
                            <?php
                    #var_dump($pageRecords);
                    if(!empty($pageRecords))
                    {
                      foreach($pageRecords as $record)
                            {
                        ?>
                            <tr id="<?php echo $record->configid ?>">
                                <td><span class="tabledit-span tabledit-identifier"><?php echo $record->configid ?></span><input class="tabledit-input tabledit-identifier" type="hidden" name="configid" value="<?php echo $record->configid ?>" disabled=""></td>
                                <td class=""><span class="tabledit-span"><?php echo $record->configname ?></span><input class="tabledit-input form-control input-sm" type="text" name="configname" value="<?php echo $record->configname ?>" style="display: none;" disabled=""></td>
                                <td class="tabledit-view-mode"><span class="tabledit-span"><?php echo $record->value ?></span><input class="tabledit-input form-control input-sm" type="text" name="value" value="<?php echo $record->value ?>" style="display: none;" disabled=""></td>
                            </tr>
                            <?php
                            }
                    }
                    ?>
                        </tbody>
                    </table>
                </div>
            
        </div>

    </div>
    </section>
</div>


<script>
$(function () {

    $('#example1').Tabledit({
        url: '<?php echo base_url($this->adminurl)?>/setting/editupdate',
        columns: {
            identifier: [0, 'id'],
            editable: [[2, 'value']]
        }
    });

  });
</script>