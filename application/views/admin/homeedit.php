<?php
$contents="";
$lastupdate = "";

if(!empty($homeInfo)){
    $contents = $homeInfo['0']->contents;
    $lastupdate = $homeInfo['0']->lastupdate;
}
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-desktop"></i> ホーム画面管理
        <small>Edit HOME</small>
      </h1>
    </section>
    
    <section class="content">
        <div class="row">
             <!-- left column -->
             <div class="col-md-12">
                
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
    

           
              <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Home画面</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    
                    <form role="form" action="<?php echo base_url($this->adminurl) ?>/home/editPost" method="post" id="editPost" role="form">
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="content" placeholder="コンテンツ" id="editor1" name="editor1" rows="10" cols="80"><?php echo $contents; ?></textarea>
                                          
                                    </div>
    
                                </div>
                                <!-- /.col-->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="postdata">最終更新日時</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control required" id="postdata" value="<?php echo $lastupdate; ?>" disabled="disabled">
                                        </div>
                                          <!-- /.input group -->
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="更新" />
                            
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>