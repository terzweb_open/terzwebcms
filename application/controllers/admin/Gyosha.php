<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/AdminController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Gyosha extends AdminController
{
    
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->isLoggedIn();
        $this->load->model('gyosha_model');
        

    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
            
            
    {
        
        
        $this->postList();
    }
    
    /**
     * This function is used to load the user list
     */
    function postList()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->gyosha_model->postsListingCount($searchText);

			$returns = $this->paginationCompress ( "pagetList/", $count, 10 );
            
            $data['pageRecords'] = $this->gyosha_model->userListing($searchText, $returns["page"], $returns["segment"]);
            $this->load_options();
            $data['status'] = $this->data;
            
            $this->global['pageTitle'] = 'ページリスト';
            
            $this->loadViews("admin/gyosha", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
         
            $this->load_options();
            $data['status'] = $this->data;
            
            $this->global['pageTitle'] = 'Add New Gyosha';
            $this->foot['ckediters_footer'] = '
                    <script src="'.  base_url().'assets/plugins/ckeditor/ckeditor.js"></script>
                    ';
            $this->global['ckediters_script'] = '<script>
  $(function (){
   CKEDITOR.replace("editor1");
   CKEDITOR.config.height = "500px"; //高さ
   CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
   CKEDITOR.config.allowedContent = true;
   	CKEDITOR.config.filebrowserImageBrowseUrl = "'.  base_url().'/assets/plugins/kcfinder/browse.php?type=images";
	CKEDITOR.config.filebrowserImageUploadUrl = "'.  base_url().'/assets/plugins/kcfinder/upload.php?type=images";
	CKEDITOR.config.filebrowserBrowseUrl = "'.  base_url().'/assets/plugins/kcfinder/browse.php?type=files";
	CKEDITOR.config.filebrowserUploadUrl = "'.  base_url().'/assets/plugins/kcfinder/upload.php?type=files";
    //bootstrap WYSIHTML5 - text editor
  });
</script>';
            $this->loadViews("admin/gyoshaNew", $this->global, $data, $this->foot);
        }
    }

    /**
     * This function is used to check whether email already exist or not
     */

    
    /**
     * This function is used to add new user to the system
     */
    function addnewgyosha()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('gname','業者名','trim|required');
            $this->form_validation->set_rules('content','掲載内容','required');
            $this->form_validation->set_rules('prnaiyou','PR文章','trim|required');
            $this->form_validation->set_rules('emails','受信アドレス','trim|required');
            $this->form_validation->set_rules('status','ステータス','trim|required|numeric');
            
            
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $gname = $this->security->xss_clean($this->input->post('gname'));
                $content = html_escape($this->input->post('content'));
                $prnaiyou = $this->security->xss_clean($this->input->post('prnaiyou'));
                $emails = $this->security->xss_clean($this->input->post('emails'));
                $status = $this->input->post('status');
                
                
                $userInfo = array(
                    'gname'=>$gname,
                    'contents'=>$content,
                    'prnaiyou'=>$prnaiyou,
                    'emails'=>$emails,
                    'status'=>$status
                    );
                

                $result = $this->gyosha_model->addNewPost($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('成功', '新規業者を登録に成功しました');
                }
                else
                {
                    $this->session->set_flashdata('失敗', '新規業者登録に失敗しました');
                }
                
                redirect($this->adminurl.'/gyosha');
            }
        }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($userId = NULL)
    {
            //システムアドミニは編集させないってか！！！ || $userId == 1　違った自分のIDって事ね
        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($userId == null)
            {
                redirect('pageList');
            }
            
            
            $data['pageInfo'] = $this->gyosha_model->getPageInfo($userId);
            
            $this->load_options();
            $data['status_list'] = $this->data;
            
            $this->global['pageTitle'] = '投稿編集';
            
            $this->foot['ckediters_footer'] = '
                    <script src="'.  base_url().'assets/plugins/ckeditor/ckeditor.js"></script>
                    ';
            $this->global['ckediters_script'] = '<script>
  $(function (){
   CKEDITOR.replace("editor1");
   CKEDITOR.config.height = "500px"; //高さ
   CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
   CKEDITOR.config.allowedContent = true;
   	CKEDITOR.config.filebrowserImageBrowseUrl = "'.  base_url().'/assets/plugins/kcfinder/browse.php?type=images";
	CKEDITOR.config.filebrowserImageUploadUrl = "'.  base_url().'/assets/plugins/kcfinder/upload.php?type=images";
	CKEDITOR.config.filebrowserBrowseUrl = "'.  base_url().'/assets/plugins/kcfinder/browse.php?type=files";
	CKEDITOR.config.filebrowserUploadUrl = "'.  base_url().'/assets/plugins/kcfinder/upload.php?type=files";
    //bootstrap WYSIHTML5 - text editor
  });
</script>';
            $this->loadViews("admin/gyoshaeditOld", $this->global, $data, $this->foot);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editPost()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $userid = $this->input->post('id');
            
            $this->form_validation->set_rules('gname','業者名','trim|required');
            $this->form_validation->set_rules('content','掲載内容','required');
            $this->form_validation->set_rules('prnaiyou','PR文章','trim|required');
            $this->form_validation->set_rules('emails','受信アドレス','trim|required');
            
            $this->form_validation->set_rules('status','ステータス','trim|required|numeric');
            $this->form_validation->set_rules('opendata','公開日時','required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($userid);
            }
            else
            {
                $gname = $this->security->xss_clean($this->input->post('gname'));
                $content = html_escape($this->input->post('content'));
                $prnaiyou = $this->security->xss_clean($this->input->post('prnaiyou'));
                $emails = $this->security->xss_clean($this->input->post('emails'));
                
                $status = $this->input->post('status');
                $opendata = $this->security->xss_clean($this->input->post('opendata'));
                
                $userInfo = array(
                    'gname'=>$gname,
                    'contents'=>$content,
                    'prnaiyou'=>$prnaiyou,
                    'emails'=>$emails,
                    
                    'status'=>$status,
                    'opendate'=>$opendata
                    );
                
                $result = $this->gyosha_model->editPost($userInfo, $userid);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', '業者情報を更新しました');
                }
                else
                {
                    $this->session->set_flashdata('error', '業者情報の更新に失敗');
                }
                
                redirect($this->adminurl.'/gyosha/editOld/'.$userid.'');
            }
        }
    }
    
    
    function deletePost()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('postId');
            #$userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            $intoData = array('isDeleted'=>1);
            
            $result = $this->gyosha_model->deletePost($userId, $intoData);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }



    

}

?>