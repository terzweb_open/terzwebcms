<?php

/* 
各ファイル読み込む
 * 
 */
?>
<!DOCTYPE html>
<html lang="jp">
{meta}
<body>
    <div class="blog">
            {blog_header}
        <!--
			Container
		-->
                <div class="container">
                    {blog_content}
                </div>
        <!-- Footer -->
        {blog_footer}
            
        <!-- Scripts -->
        {blog_footer_meta}
        <!-- // Scripts -->
    </div>   
</body>
</html>


