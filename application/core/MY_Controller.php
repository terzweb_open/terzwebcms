<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MY_Controller extends CI_Controller {
    
	protected $role = '';
	protected $vendorId = '';
	protected $name = '';
	protected $roleText = '';
	protected $global = array ();
	protected $lastLogin = '';    
    
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('config_model');
        $this->load->model('posts_model');
        $this->load->model('news_model');
        $this->load->model('menu_model');
        $this->load->library('parser');
    }
    
    /*
    *
    *   テンプレート読み込み　設定
    *
    */
    protected function make_temp($content = null, $layout = '') {
        /*サイト内コンフィグ項目読み込み*/
        $this->data['theme_folder_name'] =  $this->config_model->getconfig('theme'); #テンプレートフォルダ        
        $this->data['site_title'] =  $this->config_model->getconfig('site_title'); #サイトタイトル
        $this->data['meta_keyword'] =  $this->config_model->getconfig('meta_keyword'); #キーワード
        $this->data['meta_description'] =  $this->config_model->getconfig('meta_description'); #説明
        $this->data['base_url'] = base_url();
        
        //ブログ新着表示用
        $this->data['blog_list'] = $this->posts_model->Listing('', $this->config_model->getconfig('top_blog_count'), '');
        $this->data['news_list'] = $this->news_model->Listing('', $this->config_model->getconfig('top_news_count'), '');

        //メニュー表示用
        $this->data['menu_list'] = $this->menu_model->Listing();
        
        

        if(empty($this->data['page_title'])){
            $this->data['page_title'] = '';
        }else{
            $this->data['page_title'] = $this->data['page_title'];
        }
        if(empty($this->data['meta_page_title'])){
            $this->data['meta_page_title'] = '';
        }else{
            $this->data['meta_page_title'] = ' | '.$this->data['meta_page_title'].'';
        }
        
        //通常　テンプレート用
        $this->data['meta'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/_meta',$this->data, TRUE);
        $this->data['header'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/_header',$this->data, TRUE);
        $this->data['sidebar'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/_sidebar',$this->data, TRUE);
        $this->data['rogospace'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/_rogospace',$this->data, TRUE);        
        $this->data['firstpage'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/_firstpage',$this->data, TRUE);        
        $this->data['footer'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/_footer',$this->data, TRUE);
        $this->data['footer_meta'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/_footer_meta',$this->data, TRUE);
        $this->data['page_meta'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/page_meta',$this->data, TRUE);
         
        //ブログ　テンプレート用
        $this->data['blog_meta'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/blog_meta',$this->data, TRUE);
        $this->data['blog_header'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/blog_header',$this->data, TRUE);
        $this->data['blog_sidebar'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/blog_sidebar',$this->data, TRUE);
        $this->data['blog_rogospace'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/blog_rogospace',$this->data, TRUE);        
        $this->data['blog_footer'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/blog_footer',$this->data, TRUE);
        $this->data['blog_footer_meta'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/blog_footer_meta',$this->data, TRUE);
        
        //ニュース　テンプレート用
        $this->data['news_meta'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/news_meta',$this->data, TRUE);
        $this->data['news_header'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/news_header',$this->data, TRUE);
        $this->data['news_sidebar'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/news_sidebar',$this->data, TRUE);
        $this->data['news_rogospace'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/news_rogospace',$this->data, TRUE);        
        $this->data['news_footer'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/news_footer',$this->data, TRUE);
        $this->data['news_footer_meta'] = $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/news_footer_meta',$this->data, TRUE);
        
        

        //通常　＝　''  blog = blog , News = news,　ヘッダー無し　= nos
        if($layout == ''){
            $this->data['content'] = (is_null($content)) ? '' : $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/'.$content, $this->data, TRUE);
            $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/_layout',$this->data);
        }elseif($layout == 'nos'){
            $this->data['content'] = (is_null($content)) ? '' : $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/'.$content, $this->data, TRUE);
            $this->parser->parse('themes/' .$this->data['theme_folder_name']. '/_nos_layout', $this->data);
        }elseif($layout == 'blog'){
            $this->data['blog_content'] = (is_null($content)) ? '' : $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/'.$content, $this->data, TRUE);
            $this->parser->parse('themes/' .$this->data['theme_folder_name']. '/blog_layout', $this->data);
        }elseif($layout == 'news'){
            $this->data['news_content'] = (is_null($content)) ? '' : $this->parser->parse('themes/'.$this->data['theme_folder_name'].'/'.$content, $this->data, TRUE);
            $this->parser->parse('themes/' .$this->data['theme_folder_name']. '/news_layout', $this->data);
        }
        
        
        
        
        
    }
    /*
    protected function load_theme($content = null, $layout = '') {
        $this->data['theme'] =  $this->config_model->getconfig('theme');
        $this->data['site_title'] =  $this->config_model->getconfig('site_title');
        $this->data['meta_keyword'] =  $this->config_model->getconfig('meta_keyword');
        $this->data['meta_description'] =  $this->config_model->getconfig('meta_description');
        //マガジンがある場合、ページがある場合、その他
        
        if(empty($this->data['page_title'])){
            $this->data['page_title'] = '';
        }else{
            $this->data['page_title'] = ' || '.$this->data['page_title'].' ||';
        }  
        
        $this->data['meta'] = $this->load->view('themes/'.$this->config_model->getconfig('theme').'/inc_meta',$this->data, TRUE);
        $this->data['header'] = $this->load->view('themes/'.$this->config_model->getconfig('theme').'/inc_header',$this->data, TRUE);
        $this->data['sidebar'] = $this->load->view('themes/'.$this->config_model->getconfig('theme').'/inc_sidebar',$this->data, TRUE);
        $this->data['rogospace'] = $this->load->view('themes/'.$this->config_model->getconfig('theme').'/inc_rogospace',$this->data, TRUE);
        
        $this->data['footer'] = $this->load->view('themes/'.$this->config_model->getconfig('theme').'/inc_footer',$this->data, TRUE);
        $this->data['footer_meta'] = $this->load->view('themes/'.$this->config_model->getconfig('theme').'/inc_footer_meta',$this->data, TRUE);
        

        if($layout == ''){
            $this->data['content'] = (is_null($content)) ? '' : $this->load->view('themes/'.$this->config_model->getconfig('theme').'/'.$content, $this->data, TRUE);
            $this->load->view('themes/' . $this->config_model->getconfig('theme'). '/inc_layout', $this->data);
        }elseif($layout == 'nos'){
            
            $this->data['content'] = (is_null($content)) ? '' : $this->load->view('themes/'.$this->config_model->getconfig('theme').'/'.$content, $this->data, TRUE);
            $this->load->view('themes/' . $this->config_model->getconfig('theme'). '/inc_page_layout', $this->data);
        
        }
        else{
            $this->load->view('themes/'.$this->config_model->getconfig('theme').'/'.$content, $this->data);
        }
    }
     */
    protected function bootstrap_messege($string = null,$mess = null) {
        if($string == 'errer'){
            $messegebox = '
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                '.$mess.'
              </div>
            ';
            
        }elseif($string == 'success'){
            $messegebox = '
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                '.$mess.'
              </div>
            ';
        }
        return $messegebox;
    }
    public function response($data = NULL) {
		$this->output->set_status_header ( 200 )->set_content_type ( 'application/json', 'utf-8' )->set_output ( json_encode ( $data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ) )->_display ();
		exit ();
	}
	
	/**
	 * This function used to check the user is logged in or not
	 */
	function isLoggedIn() {
		$isLoggedIn = $this->session->userdata ( 'isLoggedIn' );
		
		if (! isset ( $isLoggedIn ) || $isLoggedIn != TRUE) {
			redirect ( 'login' );
		} else {
			$this->role = $this->session->userdata ( 'role' );
			$this->vendorId = $this->session->userdata ( 'userId' );
			$this->name = $this->session->userdata ( 'name' );
			$this->roleText = $this->session->userdata ( 'roleText' );
			$this->lastLogin = $this->session->userdata ( 'lastLogin' );
			
			$this->global ['name'] = $this->name;
			$this->global ['role'] = $this->role;
			$this->global ['role_text'] = $this->roleText;
			$this->global ['last_login'] = $this->lastLogin;
		}
	}
	

	function isAdmin() {
		if ($this->role != ROLE_ADMIN) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * This function is used to check the access
	 */
	function isTicketter() {
		if ($this->role != ROLE_ADMIN || $this->role != ROLE_MANAGER) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * This function is used to load the set of views
	 */
	function loadThis() {
		$this->global ['pageTitle'] = 'Access Denied';
		$this->load->view ( 'includes/header', $this->global );
		$this->load->view ( 'access' );
		$this->load->view ( 'includes/footer' );
	}
	
	/**
	 * This function is used to logged out user from system
	 */
	function logout() {
		$this->session->sess_destroy ();
		
		redirect ( 'login' );
	}

	/**
     * This function used to load views
     * @param {string} $viewName : This is view name
     * @param {mixed} $headerInfo : This is array of header information
     * @param {mixed} $pageInfo : This is array of page information
     * @param {mixed} $footerInfo : This is array of footer information
     * @return {null} $result : null
     */
    function loadViews($viewName = "", $headerInfo = NULL, $pageInfo = NULL, $footerInfo = NULL){

        $this->load->view('includes/header', $headerInfo);
        $this->load->view($viewName, $pageInfo);
        $this->load->view('includes/footer', $footerInfo);
    }
	
	/**
	 * This function used provide the pagination resources
	 * @param {string} $link : This is page link
	 * @param {number} $count : This is page count
	 * @param {number} $perPage : This is records per page limit
	 * @return {mixed} $result : This is array of records and pagination data
	 */
	function paginationCompress($link, $count, $perPage = 10, $segment = SEGMENT) {
		$this->load->library ( 'pagination' );
        
		$config ['base_url'] = base_url () . $link;
		$config ['total_rows'] = $count;
		$config ['uri_segment'] = $segment;
		$config ['per_page'] = $perPage;
		$config ['num_links'] = 5;
		$config ['full_tag_open'] = '<nav><ul class="pagination">';
		$config ['full_tag_close'] = '</ul></nav>';
		$config ['first_tag_open'] = '<li class="arrow">';
		$config ['first_link'] = 'First';
		$config ['first_tag_close'] = '</li>';
		$config ['prev_link'] = 'Previous';
		$config ['prev_tag_open'] = '<li class="arrow">';
		$config ['prev_tag_close'] = '</li>';
		$config ['next_link'] = 'Next';
		$config ['next_tag_open'] = '<li class="arrow">';
		$config ['next_tag_close'] = '</li>';
		$config ['cur_tag_open'] = '<li class="active"><a href="#">';
		$config ['cur_tag_close'] = '</a></li>';
		$config ['num_tag_open'] = '<li>';
		$config ['num_tag_close'] = '</li>';
		$config ['last_tag_open'] = '<li class="arrow">';
		$config ['last_link'] = 'Last';
		$config ['last_tag_close'] = '</li>';
	
		$this->pagination->initialize ( $config );
		$page = $config ['per_page'];
		$segment = $this->uri->segment ( $segment );
	
		return array (
				"page" => $page,
				"segment" => $segment
		);
	}
        

    
    protected function load_options() {
    
            //User status option
            $this->data['room_su'] = array(
                0 => '',
                1 => '1',
                2 => '2',
                3 => '3',
                4 => '4',
                5 => '5',
                6 => '6',
                10 => 'それ以上',
            );
            
            $this->data['room_type'] = array(
                0 => '',
                1 => 'ワンルーム',
                2 => 'K',
                3 => 'DK',
                4 => 'LDK'
            );
            
            $this->data['monooki'] = array(
                '' => '',
                'yes' => '物置あり'
            );
            $this->data['niwa'] = array(
                '' => '',
                'yes' => '庭あり'
            );
            $this->data['chushajyo'] = array(
                '' => '',
                'yes' => '駐車場あり'
            );
    }
}

