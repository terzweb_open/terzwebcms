<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      <i class="fa fa-paint-brush"></i> ブログ管理
        <small>Add Blog</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->

          
              <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">ブログ作成</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                        <?php
                            echo form_open_multipart(base_url($this->adminurl.'/posts/addnewpost'),array('class'=>'','id'=>'addblog'));
                        ?>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">                                
                                    <div class="form-group">
                                        <label for="fname">タイトル</label>
                                        <input type="text" class="form-control required" value="<?php echo set_value('title'); ?>" id="fname" name="title" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="url">表示用URL</label> <small><?php echo base_url('post')?>/(表示用URL) となります</small>
                                        <input type="text" class="form-control required" id="url" value="<?php echo set_value('url'); ?>" name="url" maxlength="128">
                                    
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="imgurl">キャッチアップ画像</label>
                                        <input type="file" name="userfile" class="dropify" data-width="245" data-height="245" data-allowed-file-extensions="png jpg jpeg gif">
                        
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="content">コンテンツ</label>
                                        <textarea name="content" class="textarea" placeholder="会員向け投稿スペース" id="editor1" rows="10" cols="80"><?php echo set_value('content'); ?></textarea>
                                          
                                    </div>
    
                                </div>
                                <!-- /.col-->

                            </div>
                            <div class="row">
                                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="role">ステータス</label>
                                            <?php echo form_dropdown('status', $status, set_value('statusID'), array('class'=> 'form-control required')); ?>
                                    
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="postdata">*公開日</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="date" class="form-control" id="opendate" value="<?php echo set_value('opendate',date('Y-m-d')); ?>" name="opendate">
                                        </div>
                                          <!-- /.input group -->
                                        
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="postdata">公開時間</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-clock-o"></i>
                                            </div>
                                            <input type="time" class="form-control" id="opentime" value="<?php echo set_value('opentime',date('H:m')); ?>" name="opentime">
                                        </div>
                                          <!-- /.input group -->
                                        
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="　作　成　" />
                        </div>
                    </form>
                </div>
            </div>

        </div>    
    </section>
    
</div>


<script>
$(document).ready(function(){
	
	var addUserForm = $("#addblog");
	
	var validator = addUserForm.validate({
		
		rules:{
            title :{ required : true },
            url :{ required : true, remote : { url : baseURL + "/posts/checkurl", type :"post"} },
			content : { required : true },
			opendate : { required : true},
		},
		messages:{
            title :{ required : "入力必須です" },
            url :{ required : "入力必須です", remote : "使用済みURLです" },
			content : { required : "入力必須です" },
			opendate : { required : "入力必須です" },
		}
    });
    

    $('.dropify').dropify({
            // default file for the file input
            defaultFile: '',

            // max file size allowed
            maxFileSize: '2000K',

            // custom messages
            messages: {
                'default': '画像をドラックドロップまたはクリック',
                'replace': 'ドラックドロップまたはクリックで閉じて変更',
                'remove':  '取消',
                'error':   'エラー'
            },

			error: {
					'fileSize': 'ファイルサイズは {{ value }} まで.',
					'minWidth': 'The image width is too small ({{ value }}}px min).',
					'maxWidth': 'The image width is too big ({{ value }}}px max).',
					'minHeight': 'The image height is too small ({{ value }}}px min).',
					'maxHeight': 'The image height is too big ({{ value }}px max).',
					'imageFormat': 'The image format is not allowed444 ({{ value }} only).',
					'fileExtension': '拡張子は {{ value }} のみ許可.'
				}
    });
});
</script>
