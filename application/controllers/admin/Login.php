<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


require APPPATH . '/libraries/AdminController.php';

class Login extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('config_model');
        $this->load->helper('admin_helper');

    }

    /**
     * Index
     */
    public function index()
    {
        $this->isLoggedIn();
    }
    
    /**
     * login check & session check
     */
    function isLoggedIn()
    {
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            $this->data['site_title'] = $this->config_model->getconfig('site_title');
            $this->load->view('admin/login',$this->data);
        }
        else
        {
            redirect($this->adminurl.'/dashboard');
        }
    }
    
    
    /**
     * log in check
     */
    public function loginMe()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = $this->security->xss_clean($this->input->post('email'));
            $password = $this->security->xss_clean($this->input->post('password'));
            
            $result = $this->login_model->loginMe($email, $password);
            
            if(!empty($result))
            {

                $sessionArray = array('userId'=>$result->userId,                    
                                        'role'=>$result->roleId,
                                        'roleText'=>$result->role,
                                        'name'=>$result->name,
                                        'icon_url'=>$result->icon_url,
                                        'upload_image_file_manager'=> TRUE,
                                        'lastLogin'=> date('Y/m-d H:i:s'),
                                        'isLoggedIn' => TRUE
                                );
                    

                $this->session->set_userdata($sessionArray);

                unset($sessionArray['userId'], $sessionArray['isLoggedIn'], $sessionArray['lastLogin']);

                
                redirect($this->adminurl.'/home');
            }
            else
            {
                $this->session->set_flashdata('error', 'メールアドレスもしくはパスワードが違います');
                
                redirect($this->adminurl.'/');
            }
        }
    }

    /**
     * forgot password view
     */
    public function forgotPassword()
    {
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            $this->load->view('forgotPassword');
        }
        else
        {
            redirect($this->adminurl.'/dashboard');
        }
    }



    
 

}

?>