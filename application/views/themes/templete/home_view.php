<?php

/* 
 * HOME view
 */
?>

    <div id="main">


        <div class="content">
            {home_entries}
    <hr>
            <h2>Latest NEWS</h2>
            <ul>
                {news_list}
                    <li><a href="{url}">{title}</a></li>
                {/news_list}
            </ul>
    <hr>
            <h2>Latest Blog</h2>
            <ul>
                {blog_list}
                    <li><a href="{url}">{title}</a></li>
                {/blog_list}
            </ul>
        </div>

    </div>
