<?php

/* 
 * terzweb cms
 * teruhito
 */
?>
<head>
	
	<!--
		Basic
	-->
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
	<title>{page_title} | {site_title}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta name="description" content="{meta_description}" />
	<meta name="keywords" content="{meta_keyword},{site_title}" />
    <meta name="author" content="" />

		
		<link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css" integrity="sha384-nn4HPE8lTHyVtfCBi5yW9d20FjT8BJwUXyWZT9InLYax14RDjBj46LmSztkmNP9w" crossorigin="anonymous">
        <!--[if lte IE 8]>
            <link rel="stylesheet" href="{base_url}assets/{theme_folder_name}/css/side-menu-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
            <link rel="stylesheet" href="{base_url}assets/{theme_folder_name}/css/side-menu.css">
        <!--<![endif]-->
</head>