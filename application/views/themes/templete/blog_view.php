<?php

/* 
 * Blog view
 * 
 * * Available tag
 * {blog_title}
 * {blog_img_url}
 * {blog_opendate}
 * {blog_opendate_all}
 * {blog_opendate_wareki}
 * {img_url}
 * {blog_active}
 * {blog_entries}
 */

?>

        <!--
                Blog Single
        -->
        <div class="content blog-single">

                <!-- content -->
                <div class="row">
                        <div class="col col-m-12 col-t-12 col-d-12 col-d-lg-12">
                                <div class="post-box card-box">

                                        <!-- blog detail -->
                                        <img src="{blog_img_url}">
                                        <h1>{blog_title}</h1>						
                                        <div class="blog-detail">Posted {blog_opendate_wareki}</div> 

                                        <!-- blog content -->
                                        <div class="blog-content">
                                                {blog_entries}
                                        </div>

                                </div>
                        </div>
                </div>

        </div>
