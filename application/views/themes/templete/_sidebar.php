<?php

/* 
 * terzweb cms
 * teruhito
 */
?>
    <!-- Menu toggle -->
    <a href="#menu" id="menuLink" class="menu-link">
        <!-- Hamburger icon -->
        <span></span>
    </a>
    <div id="menu">
        <div class="pure-menu">
            <a class="pure-menu-heading" href="#">{site_title}</a>
            
            <ul class="pure-menu-list">
                {menu_list}

                <li class="pure-menu-item"><a href="{url}" class="pure-menu-link">{value}</a></li>

                {/menu_list}                   

                <li class="pure-menu-item menu-item-divided pure-menu-selected">
                    <a href="#" class="pure-menu-link">Services</a>
                </li>
            </ul>
        </div>
    </div>