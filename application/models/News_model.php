<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends CI_Model
{
    var $table = 'tbl_news';

    /**
     * Count　news
     */
    function postsListingCount($searchText = '')
    {
        $this->db->from($this->table.' as BaseTbl');
        $this->db->where('BaseTbl.status !=', 3);
        $query = $this->db->get();
        
        return $query->num_rows();
    }

     /**
     * Count　news
     */
    function Listing($searchText = '', $page, $segment)
    {
        $this->db->from($this->table.' as BaseTbl');
    
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.status', 2);
        $this->db->where('BaseTbl.opendate <',date("Y-m-d H:i:s"));
        $this->db->order_by('BaseTbl.opendate', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $data1 = $query->result_array();   

        if(!empty($data1)){

            //ニュース展開
            $temp = array();
            $i=0;
            foreach ($data1 as $postitem)
            {
                $temp[$i]['title'] = $postitem['title'];
                $temp[$i]['url'] = base_url('news/').$postitem['id'];
                $temp[$i]['contents'] = mb_substr(strip_tags(html_entity_decode($postitem['contents'])),0,40);
                list($year, $month, $day, $time) = preg_split(("/[\s,-]/"), $postitem['opendate']);
                $temp[$i]['open_month'] = $month;
                $temp[$i]['open_day'] = $day;
                $temp[$i]['opendate_wareki'] = ''.$year.'年'.$month.'月'. $day .'日';
                $i++;
            }
        
        }else{

            $temp = array();

        }
        
        
        return $temp;
    }
    
 
    
    
    /**
     *  ニュース内容　参照：　削除されていない、　下書きでない、　期間中
     */
    

    function find_by_id($id)
    {
        $this->db->where('id',$id);
        $this->db->where('isDeleted',0);

            $this->db->where('status',2);
            $this->db->where('opendate <',date("Y-m-d H:i:s"));

        $query = $this->db->get($this->table);
        return $query->result_array();
    }
    
    
 

}

  