<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
# $route['default_controller'] = "login";
$route['default_controller'] = "Welcome";
$route['404_override'] = 'error';

/*フロント・サイド*/
//blog
$route['post'] = "post";
$route['post/page'] = "post";
$route['post/page/:num'] = "post/page/$1";
$route['post/(:any)'] = "post/id/$1";
//news
$route['news/:num'] = "news/id/$1";
$route['question'] = "question";
$route['contact'] = "contact";

$route['sitemap\.xml'] = "Sitemap/index";

/*********** Admin route *******************/


$route[$this->config->item('adminurl')] = 'admin/login';
$route[$this->config->item('adminurl').'/loginMe'] = 'admin/login/loginMe';
$route[$this->config->item('adminurl').'/logout'] = 'admin/logout';
$route[$this->config->item('adminurl').'/checkEmailExists'] = 'admin/checkEmailExists';

//プロフィール
$route[$this->config->item('adminurl').'/profile'] = 'admin/profile';
$route[$this->config->item('adminurl').'/profile/editPost'] = 'admin/profile/editPost';

//ホーム
$route[$this->config->item('adminurl').'/home'] = 'admin/home';
$route[$this->config->item('adminurl').'/home/editPost'] = "admin/home/editPost";
//ニュース
$route[$this->config->item('adminurl').'/news'] = 'admin/news';
$route[$this->config->item('adminurl').'/news/page'] = 'admin/news/page/';
$route[$this->config->item('adminurl').'/news/page/(:num)'] = 'admin/news/page/$1';
$route[$this->config->item('adminurl').'/news/addNew'] = 'admin/news/addNew';
$route[$this->config->item('adminurl').'/news/addNewNews'] = 'admin/news/addNewNews';
$route[$this->config->item('adminurl').'/news/addnewpost'] = 'admin/news/addnewpost';
$route[$this->config->item('adminurl').'/news/editOld/(:num)'] = "admin/news/editOld/$1";
$route[$this->config->item('adminurl').'/news/editNews'] = "admin/news/editNews";
$route[$this->config->item('adminurl').'/news/deleteNews'] = "admin/news/deleteNews";

//ブログ
$route[$this->config->item('adminurl').'/posts'] = 'admin/posts';
$route[$this->config->item('adminurl').'/posts/page'] = 'admin/posts/page/';
$route[$this->config->item('adminurl').'/posts/page/(:num)'] = 'admin/posts/page/$1';
$route[$this->config->item('adminurl').'/posts/addNew'] = 'admin/posts/addNew';
$route[$this->config->item('adminurl').'/posts/addnewpost'] = 'admin/posts/addnewpost';
$route[$this->config->item('adminurl').'/posts/editOld/(:num)'] = "admin/posts/editOld/$1";
$route[$this->config->item('adminurl').'/posts/editPost'] = "admin/posts/editPost";
$route[$this->config->item('adminurl').'/posts/deletePost'] = "admin/posts/deletePost";
$route[$this->config->item('adminurl').'/posts/checkurl'] = "admin/posts/checkurl";
$route[$this->config->item('adminurl').'/posts/recheckurl'] = "admin/posts/recheckurl";

//ページ
$route[$this->config->item('adminurl').'/page'] = 'admin/page';
$route[$this->config->item('adminurl').'/page/page'] = 'admin/page/page/';
$route[$this->config->item('adminurl').'/page/page/(:num)'] = 'admin/page/page/$1';
$route[$this->config->item('adminurl').'/page/addNew'] = 'admin/page/addNew';
$route[$this->config->item('adminurl').'/page/addnewpage'] = 'admin/page/addnewpage';
$route[$this->config->item('adminurl').'/page/editOld/(:num)'] = "admin/page/editOld/$1";
$route[$this->config->item('adminurl').'/page/editPost'] = "admin/page/editPost";
$route[$this->config->item('adminurl').'/page/deletePage'] = "admin/page/deletePost";
$route[$this->config->item('adminurl').'/page/checkurl'] = "admin/page/checkurl";
$route[$this->config->item('adminurl').'/page/recheckurl'] = "admin/page/recheckurl";

//メニュー
$route[$this->config->item('adminurl').'/menus'] = 'admin/menus';
$route[$this->config->item('adminurl').'/menus/editupdate'] = 'admin/menus/editupdate';
$route[$this->config->item('adminurl').'/menus/addNew'] = 'admin/menus/addNew';
$route[$this->config->item('adminurl').'/menus/adding'] = 'admin/menus/adding';


//設定
$route[$this->config->item('adminurl').'/setting'] = 'admin/setting';
$route[$this->config->item('adminurl').'/setting/editupdate'] = 'admin/setting/editupdate';
$route[$this->config->item('adminurl').'/setting/addNew'] = 'admin/setting/addNew';
$route[$this->config->item('adminurl').'/setting/adding'] = 'admin/setting/adding';



$route['admin/(:any)'] ='errors/error_404';
$route['admin/(:any)/(:any)'] ='errors/error_404';
$route['admin/(:any)/(:any)/(:any)'] ='errors/error_404';

/*フロント・サイド*/
$route['(:any)'] = "page/id/$1";