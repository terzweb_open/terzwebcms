<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         <i class="fa fa-paint-brush"></i> ブログ管理
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url($this->adminurl); ?>/posts/addNew"><i class="fa fa-plus"></i> ブログ作成</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">ブログリスト</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url($this->adminurl) ?>/posts" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="検索"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>URL</th>
                      <th>タイトル</th>
                      <th>ステータス</th>
                      <th>公開日時</th>
                      <th>最終更新日</th>
                      <th class="text-center">操作</th>
                    </tr>
                    <?php
                    if(!empty($postsRecords))
                    {
                        $this->load->helper(array('form', 'url'));
                        foreach($postsRecords as $record)
                        {
                    ?>
                    <tr>
                      <td><?php echo urldecode($record->url); ?></td>
                      <td><?php echo $record->title ?></td>
                      <td><?php echo $status[$record->status]?></td>
                      <td><?php echo $record->opendate ?></td>
                      <td><?php echo $record->postdata ?></td>
                      <td class="text-center">
                          <a class="btn btn-sm btn-info" href="<?php echo base_url($this->adminurl).'/posts/editOld/'.$record->id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                          <a class="btn btn-sm btn-danger deletePost" href="#" data-postid="<?php echo $record->id; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "/posts/page/" + value);
            jQuery("#searchList").submit();
        });



        jQuery(document).on("click", ".deletePost", function(){
          var postid = $(this).data("postid"),
            hitURL = baseURL + "/posts/deletePost",
            currentRow = $(this);
          
          var confirmation = confirm("ブログを削除してよいですか？");
          
          if(confirmation)
          {
            jQuery.ajax({
            type : "POST",
            dataType : "json",
            url : hitURL,
            data : { id : postid } 
            }).done(function(data){
              console.log(data);
              currentRow.parents('tr').remove();
              if(data.status = true) { alert("ブログを削除しました"); }
              else if(data.status = false) { alert("ブログを削除できませんでした"); }
              else { alert("Access denied..!"); }
            });
          }
        });
  

    });
</script>
