<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/AdminController.php';


class Page extends AdminController
{
    

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        $this->load->model('admin_page_model');
        

    }
    
    public function index(){       
        
        $this->page();
    }
    

    function page()
    {
       
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->admin_page_model->postsListingCount($searchText);

			$returns = $this->paginationCompress ( "/page/page/", $count, 10, 4 );
            
            $data['pageRecords'] = $this->admin_page_model->Listing($searchText, $returns["page"], $returns["segment"]);
            
            $this->load_options();
            $data['status'] = $this->data['status'];
            
            $this->global['pageTitle'] = 'ページリスト';
            $this->foot['editor'] = 'off';
            
            $this->loadViews("admin/page", $this->global, $data, $this->foot);
       
    }

    /**
     * new page view
     */
    function addNew()
    { 
         
            $this->load_options();
            $data['status'] = $this->data;
            
            $this->global['pageTitle'] = '新規ページ';
            
            $this->loadViews("admin/pageNew", $this->global, $data, '');
        
    }
    /**
     * new page post
     */
    function addNewPage()
    {

            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('title','タイトル','trim|required');
            $this->form_validation->set_rules('url','表示用URL','trim|required|is_unique[tbl_pages.url]',
            array(
                'is_unique' => '%sは使用済みです',
            ));
            $this->form_validation->set_rules('content','コンテンツ','required');
            $this->form_validation->set_rules('status','ステータス','trim|required|numeric');
            $this->form_validation->set_rules('opendate','公開日','required');
            $this->form_validation->set_rules('opentime','公開時間','required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $title = $this->security->xss_clean($this->input->post('title'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $content = html_escape($this->input->post('content'));
                $status = $this->input->post('status');
                $opendate = $this->security->xss_clean($this->input->post('opendate')).' '.$this->security->xss_clean($this->input->post('opentime'));
                
                $insertdata = array(
                    'title'=>$title,
                    'url'=>$url, 
                    'contents'=>$content,
                    'status'=>$status,
                    'opendate'=>$opendate
                    );
                

                $result = $this->admin_page_model->addNewPost($insertdata);


                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', '新規作成に成功しました');
                }
                else
                {
                    $this->session->set_flashdata('error', '新規作成に失敗しました');
                }
                
                redirect($this->adminurl.'/page/editOld/'.$result);
            }
        
    }

    
    /**
     * Edit　view
     * 
     */
    function editOld($pageId = NULL)
    {

            if($pageId == null)
            {
                show_404();
            }
            
            
            
            $this->load_options();
            $data['status_list'] = $this->data;            
            
            $data['pageInfo'] = $this->admin_page_model->getPageInfo($pageId);
            
            $this->global['pageTitle'] = 'ページ編集';   
            $this->loadViews("admin/pageeditOld", $this->global, $data, '');
        
    }
    
    
    /**
     * update edit
     */
    function editPost()
    {

            $this->load->library('form_validation');
            
            $pageid = $this->input->post('id');
            
            $this->form_validation->set_rules('title','タイトル','trim|required');
            $this->form_validation->set_rules('hidden-url','元URL','trim|required');
            $this->form_validation->set_rules('url','表示用URL','trim|required|callback_reurlcheck');
            $this->form_validation->set_rules('content','コンテンツ','required');            
            $this->form_validation->set_rules('status','ステータス','trim|required|numeric');
            $this->form_validation->set_rules('opendata','公開日時','required');
            $this->form_validation->set_rules('opentime','公開時間','required');
            
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($pageid);
            }
            else
            {
                $title = $this->security->xss_clean($this->input->post('title'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $content = html_escape($this->input->post('content'));
                
                $status = $this->input->post('status');
                $opendate = $this->security->xss_clean($this->input->post('opendata')).' '.$this->security->xss_clean($this->input->post('opentime'));
                
                $userInfo = array(
                    'title'=>$title,
                    'url'=>$url, 
                    'contents'=>$content,
                    
                    'status'=>$status,
                    'opendate'=>$opendate
                    );
                
                $result = $this->admin_page_model->editPost($userInfo, $pageid);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', '更新しました');
                }
                else
                {
                    $this->session->set_flashdata('error', '更新失敗');
                }
                
                redirect($this->adminurl.'/page/editOld/'.$pageid.'');
            }
        
    }

    /*
    *   Ajax URL　チェック
    */
    function checkurl()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('url','URL','trim|required');
        
        if($this->form_validation->run() == FALSE)
        {
            echo(json_encode(array('status'=>FALSE)));
        }
        else
        {
            $url = $this->input->post('url');
            $return = $this->admin_page_model->checkUrlExists($url);

            if(!empty($return)){
                echo 'false';
            }else{
                echo 'true';
            }


        }
    }


    

    /*
    *   Ajax URL　check Edit
    */
    function recheckurl()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('url','URL','trim|required');
        $this->form_validation->set_rules('reurl','元URL','trim|required');
        
        if($this->form_validation->run() == FALSE)
        {
            echo(json_encode(array('status'=>FALSE)));
        }
        else
        {
            $url = $this->input->post('url');
            $reurl = $this->input->post('reurl');
            $return = $this->admin_page_model->recheckUrlExists($url,$reurl);

            if(!empty($return)){
                echo 'false';
            }else{
                echo 'true';
            }


        }
    }



    /*
    *   Form Editing URL check
    */

    function reurlcheck($url)
    {

        if(!empty($url)){

            
            $reurl = $this->input->post('hidden-url');
            $result = $this->admin_page_model->recheckUrlExists($url,$reurl);

            if(empty($result)){
                return true;
            }
            else {
                $this->form_validation->set_message('reurlcheck', '使用済URLです');
                return FALSE;
            }
    
        }

    }

    /*
    *   Delate update
    *
    */
    
    function deletePost()
    {

        $id = $this->input->post('id');
        $intData = array('isDeleted'=>3);            
        $result = $this->admin_page_model->deletePage($id, $intData);            
        if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
        else { echo(json_encode(array('status'=>FALSE))); }
        
    }



    

}

?>