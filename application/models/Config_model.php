<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Config_Model extends CI_Model {
    
    var $table = 'tbl_config';

    	function __construct(){
		parent::__construct();
	}
 
        function getconfig($configname){
            $this->db->where('configname', $configname);
            $query = $this->db->get($this->table);
            return $query->row('value');
        }
        
}