<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/AdminController.php';


class Menus extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        $this->load->model('admin_menu_model');

    }


    public function index()
    {  
        $this->list();
    }

    function list()
    {

           
            $this->load_options();
            $data['menu_status'] = $this->data['menu_status'];
 
            
            $data['pageRecords'] = $this->admin_menu_model->userListing('','');
            

            $this->global['pageTitle'] = 'メニュー管理';
            $this->foot['editor'] = 'off';

            $this->loadViews("admin/menus", $this->global, $data, $this->foot);
        
    }

    function addNew()
    {
            $this->load_options();
            $data['menu_status'] = $this->data['menu_status'];

            $this->global['pageTitle'] = 'メニュー追加';
            $this->foot['editor'] = 'off';

            $this->loadViews("admin/menusNew", $this->global, $data, $this->foot);
      
    }

    function adding()
    {

            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('value','value','trim|required');
            $this->form_validation->set_rules('url','url','trim|required');
            $this->form_validation->set_rules('sort','sort','trim|required|numeric');
            $this->form_validation->set_rules('status','status','trim|required|numeric');
         
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {

                
                $dataInfo = array(
                    'sort' => $this->security->xss_clean($this->input->post('sort')),
                    'value'=> $this->security->xss_clean($this->input->post('value')),
                    'url'=> $this->security->xss_clean($this->input->post('url')),
                    'status'=> $this->security->xss_clean($this->input->post('status')),  
                    
                    );
                

                $result = $this->admin_menu_model->addNewData($dataInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', '新規作成しました');
                }
                else
                {
                    $this->session->set_flashdata('error', '登録エラーでした');
                }
                
                redirect($this->adminurl.'/menus');
            }
        
    }



    function editupdate()
    {

            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('id','configid','trim|required|numeric');
            $this->form_validation->set_rules('value','value','trim|required');
            $this->form_validation->set_rules('url','url','trim|required');
            $this->form_validation->set_rules('sort','sort','trim|required|numeric');
            $this->form_validation->set_rules('status','status','trim|required|numeric');
            $this->form_validation->set_rules('action','action','trim|required');
         
            
            
            if($this->form_validation->run() == FALSE)
            {
                echo(json_encode(array('status'=>FALSE)));
                
            }
            else
            {
                
                $action = $this->security->xss_clean($this->input->post('action'));
                $id = $this->security->xss_clean($this->input->post('id'));
                $value = $this->security->xss_clean($this->input->post('value'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $sort = $this->security->xss_clean($this->input->post('sort'));
                $status = $this->security->xss_clean($this->input->post('status'));

                switch ($action) {
                    case 'edit':
                        # code...
                        
                        $value = $this->security->xss_clean($this->input->post('value'));
                        
                        $dataInfo = array(
                            'sort' => $sort,
                            'value'=> $value,
                            'url'=> $url,
                            'status'=> $status,               
                            );
                        
        
                        $result = $this->admin_menu_model->updateData($dataInfo,$id);
                        
                        
                        if($result == true)
                        {
                            $retrenallay = array(
                                'id'=> $id,
                                'value'=> $value,
                                'url'=> $url,
                                'status'=> $status, 
                            );
        
                            echo json_encode($retrenallay);
                        }
                        else
                        {
                            echo(json_encode(array('status'=>FALSE)));
                        }
                        
                        break;
                    
                    case 'delete':
                        # code...
                        $result = $this->admin_menu_model->deleteData($id);
                        if ($result > 0) {
                                $retrenallay = array(
                                    'id' => $id,
                                    'value'=> ''
                                );
        
                            echo json_encode($retrenallay); 
                            }
                        else {
                            $retrenallay = array(
                                'id' => $id,
                                'value'=> ''
                            );
        
                            echo json_encode($retrenallay); 
                            }


                    break;
                }





                
            }
        
    }




}