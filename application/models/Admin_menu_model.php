<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_menu_model extends CI_Model
{

    var $table = 'tbl_menu';


    /**　　リスト用件数処理     */
    function Counting()
    {
        $this->db->from($this->table);
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    /**　　リスト表示処理     */
    function userListing($page, $segment)
    {
        $this->db->from($this->table);
        $this->db->order_by('sort','ASC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }


    /**　　登録処理     */
    function addNewData($dataInfo)
    {
        $this->db->trans_start();
        $this->db->insert($this->table, $dataInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    
    /**　　更新処理     */
    function updateData($dataInfo, $ID)
    {
       
        $this->db->where('id', $ID);
       
        if( $this->db->update($this->table, $dataInfo) ){
            return true;
        }else{
            return false;
        }
    }
    
    
    
    /**　　削除処理     */
    function deleteData($dataId)
    {
        $this->db->trans_start();
        $this->db->where('id', $dataId);
        #$this->db->update('tbl_tanto', $userInfo);
        $this->db->delete($this->table);
        $this->db->trans_complete();
        if($this->db->trans_status()===false){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
            return true;
        }   
    }



}

  