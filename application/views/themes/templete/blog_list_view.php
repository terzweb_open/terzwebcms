<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!-- 
        Card - Blog
-->
<div class="card-inner blogs active" id="blog-card">
        <div class="row card-container" data-simplebar>

    <!--
            Card Wrap
    -->
    <div class="card-wrap blogs-content col col-m-12 col-t-12 col-d-9 col-d-lg-9">

            <!--
                    Inner Top
            -->
            <div class="content inner-top">
                    <div class="row">
                            <div class="col col-m-12 col-t-12 col-d-12 col-d-lg-12">
                                    <div class="title-bg">My Blog</div>
                            </div>
                    </div>
            </div>

            <!--
                    Blog
            -->
            <div class="content blog">


                    <!-- blog items -->
                    <div class="row">

                            <!-- blog item -->
                            {blog_entries}
                            <div class="col col-m-12 col-t-12 col-d-6 col-d-lg-6">
                                    <div class="box-item card-box">
                                            <div class="image">
                                                    <a href="{url}">
                                                            <img src="{img_url}" alt="" width="100"/>
                                                            <span class="info">
                                                                    <span class="icon la la-newspaper-o"></span>
                                                            </span>
                                                            <span class="date"><strong>{opendate_wareki}</strong></span>
                                                    </a>
                                            </div>
                                            <div class="desc">
                                                    <a href="{url}" class="name">{title}</a>
                                                    <div class="category"></div>
                                            </div>
                                    </div>
                            </div>
                            <hr>
                            {/blog_entries}
                            


                    </div>

                    <div class="pager">
                    {pagenation}
                    </div>

            </div> 

    </div>

   </div>
</div>

