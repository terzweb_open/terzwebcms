<?php

class Menu_Model extends CI_Model {
    
    var $table = 'tbl_menu';
	
        protected $base_assets_url = '';
    
	function __construct(){
		parent::__construct();
	}
	
	
        
        function Listing()
        {
            $this->db->from($this->table.' as BaseTbl');
            $this->db->where('BaseTbl.isDeleted', 0);
            $this->db->where('BaseTbl.status', 1);
            $this->db->order_by('BaseTbl.sort', 'ASC');
            $query = $this->db->get();

            $data1 = $query->result_array();


            if(!empty($data1)){

                //ブログリスト展開ｇ
                $temp = array();
                $i=0;
                foreach ($data1 as $postitem)
                {
                    $temp[$i]['value'] = $postitem['value'];
                    $temp[$i]['url'] = $postitem['url'];
                    $i++;
                }
            
            }else{

                $temp = array();

            }
            
            
            return $temp;
        }
        
     

}

?>
