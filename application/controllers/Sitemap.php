<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends MY_Controller {
    
    
        public function __construct()
        {
            parent::__construct();

            // 独自処理
            $this->load->helper('url');
            $this->load->model('sitemap_model');
        }
    
	public function index(){
		// echo "hello world";
		// $this->load->view("view_home");
		$this->makesitemap();	//以下で定義した、home functionを読み込む
	}
        
        public function makesitemap() {
            
            
            //HOME用
            #$this->data['lines'] = $this->station_model->find_by_prif();
            //ブログ用
            $this->data['posts'] = $this->sitemap_model->getpost();
            
            //ページ用
            $this->data['pages'] = $this->sitemap_model->getpage();

            

            $this->load->view('sitemap', $this->data);
        }
}