<?php

/* 
各ファイル読み込む
 * @terzweb.com
 */
?>
<!DOCTYPE html>
<html lang="jp">
{meta}
<body>
<div id="layout">
    {sidebar}
    {content}
    {footer}
</div>
{footer_meta}  
</body>
</html>


