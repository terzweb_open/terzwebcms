<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/AdminController.php';


class Home extends AdminController
{
    

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Tokyo');
        $this->isLoggedIn();
        $this->load->model('admin_home_model');
        $this->load->model('config_model');
        $this->load->helper('date');
        

    }
    

    public function index()
    {
        

        $this->edit();
    }
    
    /**
     * HOME編集 画面
     */
    function edit()
    {
            
            $data['homeInfo'] = $this->admin_home_model->getInfo();
            
            $this->global['pageTitle'] = 'HOME編集';
            
            $this->loadViews("admin/homeedit", $this->global, $data, '');       
    }
    
    /**
     * HOME編集 アップデート
     */    

    function editPost()
    {

            $this->load->library('form_validation');
            
            
            $this->form_validation->set_rules('content','コンテンツ','required');

            
            if($this->form_validation->run() == FALSE)
            {
                $this->edit();
            }
            else
            {

                $content = html_escape($this->input->post('content'));
                

                
                $userInfo = array(
                    'contents'=>$content,
                    'lastupdate'=> date('Y-m-d H:i:s')
                    );
                
                $result = $this->admin_home_model->editPost($userInfo);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'コンテンツを更新しました');
                }
                else
                {
                    $this->session->set_flashdata('error', 'コンテンツ更新失敗');
                }
                
                redirect($this->adminurl.'/home');
            }
      
    }



    

}

?>