<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('homes_model');
        $this->load->library('parser');
    }
/*
 * ホームコンテンツ表示
 */
	public function index()
	{
            $data['name'] = $this->input->post("id");
            // ajaxでポストしたデータをis_ajaxで受け取る
            $is_ajax = $this->input->post("ajax");
            
            $data['home_contetns'] = $this->homes_model->getInfo();

            $this->data = array(
                    'page_title'   => 'HOME',
                    'home_entries' =>  html_entity_decode($data['home_contetns']['0']['contents'])
            );

            $this->load->view('themes/ihinseiriyo/question_01',$data);
	}
}
