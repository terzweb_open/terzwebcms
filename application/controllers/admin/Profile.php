<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

require APPPATH . '/libraries/AdminController.php';

class Profile extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        $this->load->model('admin_user_model');
        $this->load->model('config_model');
        $this->load->helper('admin_helper');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->firstpage();
    }
    
    public function firstpage() {

            $data['profile'] = $this->admin_user_model->getUserInfo($_SESSION['userId']);


            if(!empty($data['profile']))
            {

                $this->global['pageTitle'] = 'プロフィール管理';
                $this->foot['editor'] = 'off';
                
                $this->loadViews("admin/profile_view", $this->global, $data, $this->foot);

            }else{
                show_404();
            }


        

    }


    /**
     * update edit
     */
    function editPost()
    {

            $this->load->library('form_validation');
            
            
            $this->form_validation->set_rules('name','名前','trim');
            $this->form_validation->set_rules('email','E-メール','trim|required|valid_email');
            $this->form_validation->set_rules('mobile','電話番号','trim|numeric');
            $this->form_validation->set_rules('password','現パスワード','required|alpha_numeric|callback_passwordcheck');
            $this->form_validation->set_rules('passwordNew','パスワード変更','trim|alpha_numeric');
            $this->form_validation->set_rules('passwordRe','パスワード変更 確認','trim|alpha_numeric|matches[passwordNew]');
            
            
            if($this->form_validation->run() == FALSE)
            {
                $this->firstpage();
            }
            else
            {


                if (!empty($_FILES['userfile']['name'])){
                    //$this->do_img_upload('images'); 
                    #$filename[$i] = $_FILES['userfile']['name'][$i];

                 

                    $_FILES['userfile']['name'];
                    $_FILES['userfile']['type'];
                    $_FILES['userfile']['tmp_name'];
                    $_FILES['userfile']['error'];
                    $_FILES['userfile']['size'];

                        $this->load->library('upload', setuploadprotocol());
                        $this->upload->initialize(setuploadprotocol());
                        
                        $return = $this->upload->do_upload();

                                              

                    if (!$return)
                        {
                            //エラー                          
                            $this->file_errors = $this->upload->display_errors();

                        }else{
                            //成功

                            $data_one = array('upload_data' => $this->upload->data());
                            $filename = $data_one['upload_data']['file_name'];
                            $imgarray = array(
                                'icon_url'=>$filename
                            );
                            $sessionArray = array(
                                'icon'=> $filename,
                                );
        
                            $this->session->set_userdata($sessionArray);
                            
                        }


                }else{
                    $imgarray = array();
                }


                //パスワード変更が入力されているか否か
                if(!empty($this->security->xss_clean($this->input->post('passwordRe'))))
                {
                    //パスワード更新
                  
                    $userInfo = array(
                        'name'=> $this->security->xss_clean($this->input->post('name')),
                        'email'=> $this->security->xss_clean($this->input->post('email')), 
                        'mobile'=> $this->security->xss_clean($this->input->post('mobile')),
                        'password'=>getHashedPassword($this->security->xss_clean($this->input->post('passwordNew'))),                  
                        'updatedBy'=> $_SESSION['userId'],
                        'updatedDtm'=> date('Y-m-d h:i:s')
                        );
                    
                    $userInfo = $imgarray + $userInfo;

                    $result = $this->admin_user_model->updatedata($userInfo);
                    
                    if($result == true)
                    {
                        //セッション更新
                        $sessionArray = array(
                        'name'=> $this->security->xss_clean($this->input->post('name')),
                        );

                        $this->session->set_userdata($sessionArray);


                        $this->session->set_flashdata('success', '更新しました'.$this->file_errors);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', '更新失敗'.$this->file_errors);
                    }

                    redirect($this->adminurl.'/profile');

                    

                }
                else
                {

                    //通常更新
                    $userInfo = array(
                        'name'=> $this->security->xss_clean($this->input->post('name')),
                        'email'=> $this->security->xss_clean($this->input->post('email')), 
                        'mobile'=> $this->security->xss_clean($this->input->post('mobile')),                    
                        'updatedBy'=> $_SESSION['userId'],
                        'updatedDtm'=> date('Y-m-d h:i:s')
                        );
                    
                    $userInfo = $imgarray + $userInfo;

                    $result = $this->admin_user_model->updatedata($userInfo);

                    
                    
                    if($result == true)
                    {
                        //セッション更新
                        $sessionArray = array(
                        'name'=> $this->security->xss_clean($this->input->post('name')),
                        );

                        $this->session->set_userdata($sessionArray);


                        $this->session->set_flashdata('success', '更新しました'.$this->file_errors);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', '更新失敗'.$this->file_errors);
                    }

                    redirect($this->adminurl.'/profile');


                }

                
            }
        
    }


    function passwordcheck($password)
    {

        //パスワードの確認
        $user = $this->admin_user_model->getUserInfo($_SESSION['userId']);

        if(verifyHashedPassword($password, $user['0']->password)){
            //認証OK
            return true;               

        } else {
            //認証　駄目
            $this->form_validation->set_message('passwordcheck', 'パスワード認証エラーです');
            return FALSE;
            
        }        

    }
    
    
}