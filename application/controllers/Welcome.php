<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('config_model');
        $this->load->model('homes_model');
        $this->load->model('posts_model');
        $this->load->library('parser');
        $this->load->library('form_validation');
        $this->theme = $this->config_model->getconfig('theme');
    }
/*
 * ホームコンテンツ表示
 */
	public function index()
	{


            #　ホーム記事取得
            $data['home_contetns'] = $this->homes_model->getInfo();

            $this->data = array(
                    'page_title'   => 'ホーム',
                    'home_entries' =>  html_entity_decode($data['home_contetns']['0']['contents']),
            );

            $this->make_temp('home_view', '');
            
	}
}
