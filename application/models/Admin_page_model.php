<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_page_model extends CI_Model
{
    var $table = 'tbl_pages';

    /**
     * count
     */
    function postsListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.status, BaseTbl.id, BaseTbl.url, BaseTbl.title, BaseTbl.contents, BaseTbl.status, BaseTbl.opendate, BaseTbl.postdata');
        $this->db->from('tbl_pages as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.title  LIKE '%".$searchText."%'
                            OR  BaseTbl.contents  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted !=', 3);
        $query = $this->db->get();
        
        return $query->num_rows();
    }

    /**
     * List
     */   
    

    function Listing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.status, BaseTbl.id, BaseTbl.url, BaseTbl.title, BaseTbl.contents, BaseTbl.opendate, BaseTbl.postdata');
        $this->db->from('tbl_pages as BaseTbl');

        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.title  LIKE '%".$searchText."%'
                            OR  BaseTbl.contents  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted !=', 3);
        $this->db->limit($page, $segment);
        $this->db->order_by('opendate','desc');
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    


    
    /**
     * new post
     * 
     */
    function addNewPost($data)
    {
        $this->db->trans_start();
        $this->db->insert($this->table, $data);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * edit info

     */
    function getPageInfo($id)
    {
        $this->db->where('id',$id);
        $query = $this->db->get($this->table,1);
        return $query->result();
    }
    
    
    /**
     * edit update
     */
    function editPost($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        
        return TRUE;
    }
    
    
    
    /**
     * delate data
     */
    function deletePage($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        
        return $this->db->affected_rows();
    }

    /**
     * check url ajax 
     */
    function checkUrlExists($url)
    {
        $this->db->from($this->table);
        $this->db->where("url", $url); 
        $this->db->where("isDeleted !=", 3);
        $query = $this->db->get();

        return $query->result();
    }

    /**
     * check url form 
     */
    function recheckUrlExists($url,$nowurl)
    {
        $this->db->from($this->table);
        $this->db->where("url", $url); 
        $this->db->where("isDeleted !=", 3);
        $this->db->where("url !=", $nowurl);
        $query = $this->db->get();

        return $query->result();
    }



}

  