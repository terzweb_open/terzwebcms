<?php

/* 
 * news page 
 */
?>
    <div id="main">
        <div class="header">
            <h1>{news_title}</h1>
        </div>

        <div class="content">
        {news_entries}
        </div>
    </div>

