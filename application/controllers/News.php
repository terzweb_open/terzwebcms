<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

/* 
 * To change this license header, choose License Headers in Project Properties.
 */

class News extends MY_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('news_model');
    }


    public function id() {

        //数字無しは　エラー        
        if (empty($this->security->xss_clean($this->uri->segment(2)))) {
            show_404();
        }else
        {

            //対象データの参照
            $data['newsdata'] = $this->news_model->find_by_id($this->security->xss_clean($this->uri->segment(2)));

                     
            //件数無しだったらページノットファウンド
            if (empty($data['newsdata'])) {
                show_404();
            }
                    
            $this->data = array(
                        'news_title'   => $data['newsdata'][0]['title'],
                        'news_entries' =>  html_entity_decode($data['newsdata']['0']['contents'])
                );
            $this->make_temp('news_view', 'news');

        }
        

    }
    
    
}