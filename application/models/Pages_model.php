<?php

class Pages_Model extends CI_Model {
    
    var $table = 'tbl_pages';
	
        protected $base_assets_url = '';
    
	function __construct(){
		parent::__construct();
	}
	
	
        function find_by_url($url) {
                $this->db->where('url', $url);
            $this->db->where('status',2);
            $this->db->where('opendate <',date("Y-m-d H:i:s"));
            
            $query = $this->db->get($this->table);
            $data = $query->result_array();

            
            if(!empty($data)){

              

                //ページ展開
                list($year, $month, $day, $time) = preg_split(("/[\s,-]/"), $data[0]['opendate']);
                $temp = array(
                    'page_title'   => $data[0]['title'],
                    'page_opendate' => $data[0]['opendate'],
                    'page_opendate_all' => date('c',strtotime($data[0]['opendate'])),
                    'page_opendate_wareki' => ''.$year.'年'.$month.'月'. $day .'日',
                    'page_active' => 'active',
                    'page_entries' =>  html_entity_decode($data['0']['contents'])

                );
            
            }else{

                $temp = array();

            }
            
            
            return $temp;
        }
   

        

        
        

}

?>
