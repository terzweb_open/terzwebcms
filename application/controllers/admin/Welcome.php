<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Welcome extends MY_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        #$this->load->library('frontcontroller');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->firstpage();
    }
    
    public function firstpage() {
        #load_thema
        $this->data['home_page'] = "";
        redirect('/admin/login');
    }
    
    
}