<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/AdminController.php';



class News extends AdminController
{
    
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        $this->load->model('admin_news_model');
        $this->load->helper('admin_helper');

    }
    

    public function index()
    {
        $this->page();
    }
    

    function page()
    {
        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->admin_news_model->newsListingCount($searchText);
            
            $returns = $this->paginationCompress ( "/news/page/", $count, 10, 4);
            
            $data['newsRecords'] = $this->admin_news_model->newsListing($searchText, $returns["page"], $returns["segment"]);

            $this->load_options();
            $data['status'] = $this->data['status'];
            
            $this->global['pageTitle'] = 'ページリスト';
            $this->foot['editor'] = 'off';
            
            $this->loadViews("admin/news_list_view", $this->global, $data, $this->foot);
       
    }

    /**
     * 新規作成画面
     */
    function addNew()
    {
         
            $this->load_options();
            $data['status'] = $this->data;
            
            $this->global['pageTitle'] = '新規ニュース';

            $this->loadViews("admin/newsNew", $this->global, $data, '');
    
    }
    
    /**
     * ニュースを追加
     */
    function addNewNews()
    {

            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('title','タイトル','trim|required|max_length[256]');
            $this->form_validation->set_rules('content','コンテンツ','required');
            $this->form_validation->set_rules('status','ステータス','trim|required|numeric');
            $this->form_validation->set_rules('opendate','公開日','required');
            $this->form_validation->set_rules('opentime','公開時間','required');
            
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $title = $this->security->xss_clean($this->input->post('title'));
                $content = html_escape($this->input->post('content'));
                $status = $this->input->post('status');
                $opendate = $this->security->xss_clean($this->input->post('opendate')).' '.$this->security->xss_clean($this->input->post('opentime'));
                
                $userInfo = array(
                    'title'=>$title,
                    'contents'=>$content,
                    'status'=>$status,
                    'opendate'=>$opendate
                    );
                

                $result = $this->admin_news_model->addNewNews($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', '新規ニュース作成しました');
                }
                else
                {
                    $this->session->set_flashdata('error', '新規ニュース作成に失敗しました');
                }
                
                redirect($this->adminurl.'/news');
            }
       
    }

    
    /**
     * 編集画面
     * 
     */
    function editOld($Id = NULL)
    {           
            if($Id == null)
            {
                show_404();
                
            }else{
            
            
            $data['newsInfo'] = $this->admin_news_model->getNewsInfo($Id);
            
            $this->load_options();
            $data['status_list'] = $this->data;
            
            $this->global['pageTitle'] = 'ニュース編集';
            
            
            $this->loadViews("admin/newseditOld", $this->global, $data, '');
            }
        
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editNews()
    {

            $this->load->library('form_validation');
            
            $id = $this->input->post('id');
            
            $this->form_validation->set_rules('title','タイトル','trim|required|max_length[128]');
            $this->form_validation->set_rules('content','コンテンツ','required');            
            $this->form_validation->set_rules('status','ステータス','trim|required|numeric');
            $this->form_validation->set_rules('opendata','公開日時','required');
            $this->form_validation->set_rules('opentime','公開時間','required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($id);
            }
            else
            {
                $title = $this->security->xss_clean($this->input->post('title'));
                $content = html_escape($this->input->post('content'));
                
                $status = $this->input->post('status');
                $opendata = $this->security->xss_clean($this->input->post('opendata')).' '.$this->security->xss_clean($this->input->post('opentime'));
                
                $dataInfo = array(
                    'title'=>$title,
                    'contents'=>$content,                    
                    'status'=>$status,
                    'opendate'=>$opendata
                    );

                
                $result = $this->admin_news_model->editNews($dataInfo, $id);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'ニュースを更新しました');
                }
                else
                {
                    $this->session->set_flashdata('error', 'ニュース更新失敗');
                }
                
                redirect($this->adminurl.'/news/editOld/'.$id.'');
            }
        
    }


    function deleteNews()
    {
        
            $id = $this->input->post('id');
            $intData = array('isDeleted'=>3);            
            $result = $this->admin_news_model->deleteNews($id, $intData);            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        
    }



    

}

?>