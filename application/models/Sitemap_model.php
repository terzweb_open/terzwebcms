<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap_Model extends CI_Model {
    
    var $tablepage = 'tbl_pages';
    var $tablepost = 'tbl_posts';
	
        protected $base_assets_url = '';
    
	function __construct(){
		parent::__construct();
	}
	
	
        function getpost() {

            $this->db->where('status',2);
            $this->db->where('isDeleted',0);
            $this->db->order_by('opendate', 'DESC');
            $query = $this->db->get($this->tablepost);
            return $query->result_array();
        }

        function getpage() {

            $this->db->where('status',2);
            $this->db->where('isDeleted',0);
            $this->db->order_by('opendate', 'DESC');
            $query = $this->db->get($this->tablepage);
            return $query->result_array();
        }

}

?>
