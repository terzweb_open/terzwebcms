<?php

class Posts_Model extends CI_Model {
    
    var $table = 'tbl_posts';
	
        protected $base_assets_url = '';
    
	function __construct(){
		parent::__construct();
	}
	
	
        function find_by_url($url) {

            $this->db->where('status',2);
            $this->db->where('url', $url);
            $this->db->where('isDeleted',0);
            $query = $this->db->get($this->table);

            $data = $query->result_array();

            
            if(!empty($data)){

              

                //ブログ展開
                list($year, $month, $day, $time) = preg_split(("/[\s,-]/"), $data[0]['opendate']);
                $temp = array(
                    'page_title'   => $data[0]['title'],
                    'blog_title' => $data[0]['title'],
                    'blog_img_url' => base_url('images/').$data[0]['img_url'],
                    'blog_opendate' => $data[0]['opendate'],
                    'blog_opendate_all' => date('c',strtotime($data[0]['opendate'])),
                    'blog_opendate_wareki' => ''.$year.'年'.$month.'月'. $day .'日',
                    'img_url' => $data[0]['img_url'],
                    'blog_active' => 'active',
                    'blog_entries' =>  html_entity_decode($data['0']['contents'])
                );
            
            }else{

                $temp = array();

            }
            
            
            return $temp;
        }

        function ListingCount($searchText = '')
        {
            $this->db->select('BaseTbl.url, BaseTbl.title, BaseTbl.contents, BaseTbl.contents_sub, BaseTbl.opendate');
            $this->db->from('tbl_posts as BaseTbl');
            if(!empty($searchText)) {

                $data2 = mb_convert_kana($searchText, 's', 'UTF-8');
                $arrayword = explode(" ", $data2);

                $this->db->group_start();
                //OR検索
                foreach ($arrayword as $key => $value) {
                    # 検索ワードを追加
              
                    $this->db->or_like('title', $value);    //ライク検索
                    $this->db->or_like('contents', $value);
                    $this->db->or_like('contents_sub', $value);

                }
                $this->db->group_end();

            }
            $this->db->where('BaseTbl.isDeleted', 0);
            $this->db->where('BaseTbl.status', 2);
            $query = $this->db->get();

        
        return $query->num_rows();
        }
        
        function Listing($searchText = '', $page, $segment)
        {
            $this->db->select('BaseTbl.url, BaseTbl.title, BaseTbl.img_url, BaseTbl.contents, BaseTbl.contents_sub, BaseTbl.opendate');
            $this->db->from('tbl_posts as BaseTbl');
            if(!empty($searchText)) {

                $data2 = mb_convert_kana($searchText, 's', 'UTF-8');
                $arrayword = explode(" ", $data2);

                $this->db->group_start();
                //OR検索
                foreach ($arrayword as $key => $value) {
                    # 検索ワードを追加
              
                    $this->db->or_like('title', $value);    //ライク検索
                    $this->db->or_like('contents', $value);
                    $this->db->or_like('contents_sub', $value);

                }
                $this->db->group_end();

            }
            $this->db->where('BaseTbl.isDeleted', 0);
            $this->db->where('BaseTbl.status', 2);
            $this->db->where('BaseTbl.opendate <',date("Y-m-d H:i:s"));
            $this->db->order_by('BaseTbl.opendate', 'DESC');
            $this->db->limit($page, $segment);
            $query = $this->db->get();

            $data1 = $query->result_array();


            if(!empty($data1)){

                //ブログリスト展開ｇ
                $temp = array();
                $i=0;
                foreach ($data1 as $postitem)
                {
                    $temp[$i]['title'] = $postitem['title'];
                    $temp[$i]['url'] = base_url('post/').$postitem['url'];
                    $temp[$i]['img_url'] = base_url('images/'.$postitem['img_url']); //画像URL
                    $temp[$i]['contents'] = mb_substr(strip_tags(html_entity_decode($postitem['contents'])),0,40);
                    list($year, $month, $day, $time) = preg_split(("/[\s,-]/"), $postitem['opendate']);
                    $temp[$i]['open_month'] = $month;
                    $temp[$i]['open_day'] = $day;
                    $temp[$i]['opendate_wareki'] = ''.$year.'年'.$month.'月'. $day .'日';
                    $i++;
                }
            
            }else{

                $temp = array();

            }
            
            
            return $temp;
        }
        
     

}

?>
