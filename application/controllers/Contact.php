<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

class Contact extends MY_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('recaptcha');
        $this->load->model('config_model');
        $this->data['page_title'] = 'お問い合わせ';
        $this->data['meta_page_title'] = 'お問い合わせ';
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->firstpage();
    }
    
    public function firstpage() 
    {
        $this->data['recaptcha'] = $this->recaptcha->create_box();
        
        
        $this->make_temp('contact_view', 'nos');
    }
    
    public function validation() 
    {
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name','お名前','required');
        $this->form_validation->set_rules('email','メールアドレス','trim|required|valid_email');
        $this->form_validation->set_rules('naiyou','お問い合わせ','required');
        
         if($this->form_validation->run() == FALSE)
         {
                $this->firstpage();
         }
         else
         {
             //すばらしい　recaptcha シンプルで大変つかいやすい
             $is_valid = $this->recaptcha->is_valid();
             

             #$is_valid['success'] = TRUE;

			if($is_valid['success'])
			{
                            //ヘルパー読み込み
                            $this->load->helper('admin_helper');

                            
                            //メール送信
                            $name = $this->security->xss_clean($this->input->post('name'));
                            $email = $this->security->xss_clean($this->input->post('email'));
                            
                            
                            $line = explode("\n",$this->input->post('naiyou'));

                            $txt = "";

                            foreach ($line as $k=>$v) {

                            $txt .= htmlspecialchars($v)."<br />";

                            }
                            
                             $naiyou = $txt;
                                                    
                            
                            $data1["email"] = $email;
                            $data1["contact_name"] = $name;
                            $data1["contact_email"] = $email;
                            $data1["contact_naiyou"] = $naiyou;
                            
                            $data1["bccemail"] = array(
                                    'terreto@terreto.work'
                                );
                            
                            $sendStatus = completeContactEmail($data1);

                            
                             
                             if($sendStatus){
                                 $status = "success";
                                 setFlashData($status, "お問い合わせ内容を送信しました");
                                 redirect('contact');
                             } else {
                                 $status = "error";
                                 setFlashData($status, "送信に失敗したようです。");
                                 redirect('contact');
                             }
				#echo "reCAPTCHA solved";
                                
			}
			else
			{
                            $this->session->set_flashdata('error', '「私はロボットではありません」にチェックをお願いします');
                            $this->firstpage();
			}
             
         }
        
    }
    
    public function feedback() {
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name','お名前','required');
        $this->form_validation->set_rules('email','メールアドレス','trim|required|valid_email');
        $this->form_validation->set_rules('message','メッセージ','required');
         
        if($this->form_validation->run() == FALSE)
         {
                
         }
         else
         {
             
             //ヘルパー読み込み
                            $this->load->helper('admin_helper');

                            
                            //メール送信
                            $name = $this->security->xss_clean($this->input->post('name'));
                            $email = $this->security->xss_clean($this->input->post('email'));
                            
                            
                            $line = explode("\n",$this->input->post('message'));

                            $txt = "";

                            foreach ($line as $k=>$v) {

                            $txt .= htmlspecialchars($v)."<br />";

                            }
                            
                             $naiyou = $txt;
                                                    
                            
                            $data1["email"] = $email;
                            $data1["contact_name"] = $name;
                            $data1["contact_email"] = $email;
                            $data1["contact_naiyou"] = $naiyou;
                            
                            $data1["bccemail"] = array(
                                    'terzweb@gmail.com'
                                );


                            $sendStatus = completeContactEmail($data1);
                            
                            if(!$sendStatus){
                                print json_encode(array('status'=>0));
                            } else {
                                print json_encode(array('status'=>1));
                            }
             
         }
        

        
        
        
    }
    public function freemail_check($str)
        {
                $code = array("@gmail.com", "@yahoo.co.jp", "@hotmail.com","@docomo.ne.jp","@softbank.ne.jp","@au.com");


                foreach ($code as $keyword) {
                    if (stripos($str, $keyword) !== false) {
                        $this->form_validation->set_message('freemail_check', '{field} 欄に "'.$keyword.'" は使えません');
                        return FALSE;
                    }
                }
                return true;
                
        }
    
}