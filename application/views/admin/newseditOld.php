<?php

$id = '';
$title = '';
$contents = '';
$status = '';
$opendate = '';

if(!empty($newsInfo))
{
    foreach ($newsInfo as $uf)
    {
        $id = $uf->id;
        $title = $uf->title;
        $contents = $uf->contents;
        $status = $uf->status;
        $opendate = $uf->opendate;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> ニュース管理
        <small>Edit News</small>
      </h1>
    </section>
    
    <section class="content">



        <div class="row">
            <!-- left column -->
            <div class="col-md-12">

                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>


                <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>

              <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">ニュース編集</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    
                    <form role="form" action="<?php echo base_url($this->adminurl) ?>/news/editNews" method="post" id="editPost" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">                                
                                    <div class="form-group">
                                        <label for="fname">タイトル</label>
                                        <input type="text" class="form-control required" value="<?php echo $title; ?>" id="fname" name="title" maxlength="128">
                                        <input type="hidden" value="<?php echo $id; ?>" name="id" /> 
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="content">コンテンツ</label>
                                        <textarea name="content" id="editor1" name="editor1" rows="10" cols="80"><?php echo $contents; ?></textarea>
                                          
                                    </div>
    
                                </div>
                                <!-- /.col-->
                            </div>
                            <div class="row">
                                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="role">ステータス</label>
                                            <?php echo form_dropdown('status', $status_list, $status, array('class'=> 'form-control required')); ?>
                                    
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="postdata">公開日</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="date" class="form-control" value="<?php echo set_value('opendata',substr($opendate, 0, 10)); ?>" name="opendata">
                                        
                                        </div>
                                          <!-- /.input group -->
                                        
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="postdata">公開時間</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-clock-o"></i>
                                            </div>
                                            <input type="time" class="form-control" id="opentime" value="<?php echo set_value('opentime',substr($opendate, 11, -3)); ?>" name="opentime">
                                        </div>
                                          <!-- /.input group -->
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="更新" />
                            
                        </div>
                    </form>
                </div>
            </div>

        </div>    
    </section>
</div>

<script>
$(document).ready(function(){
	
	var addUserForm = $("#editPost");
	
	var validator = addUserForm.validate({
		
		rules:{
			title :{ required : true },
			content : { required : true },
			opendate : { required : true},
		},
		messages:{
			title :{ required : "入力必須です" },
			content : { required : "入力必須です" },
			opendate : { required : "入力必須です" },
		}
	});
});
</script>