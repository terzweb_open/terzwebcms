<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Page extends MY_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pages_model');

    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->firstpage();
    }
    
    public function firstpage() {
        $this->uri->segment(1);
        #load_thema

        $this->make_temp('page_view', '');
    }
    public function id() {
        
        $urltitle = urldecode($this->uri->segment(1));
        
        //テーブルpagesのtitleと参照
        
        $data['pagedata'] = $this->pages_model->find_by_url($urltitle);
        
        //件数無しだったらページノットファウンド
        if (empty($data['pagedata'])) {
            show_404($page = '', $log_error = TRUE);
        }
                
        $this->data = $data['pagedata'];
        
        $this->make_temp('page_view', '');
    }
    
    
}