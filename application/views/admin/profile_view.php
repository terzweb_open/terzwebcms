<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user"></i> プロフィール管理
        <small>Edit Profile</small>
      </h1>
    </section>
    
    <section class="content">

               

        <div class="row">
            <!-- left column -->
            <div class="col-md-12">

                 <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
            

                <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>


              <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">プロフィール編集</h3>
                    </div><!-- /.box-header -->

                    <!-- form start -->
                    <?php
                        echo form_open_multipart(base_url($this->adminurl.'/profile/editPost'),array('class'=>'','id'=>'editPost'));
                    ?>
                    
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12"> 
                 
                                    <div class="form-group">
                                        <label for="fname">名前</label>
                                        <input type="text" class="form-control" value="<?php echo set_value('name',$profile[0]->name); ?>" id="fname" name="name" maxlength="128">
                                        
                                    </div>
                                    
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="url">*E-メール</label>
                                        <input type="text" class="form-control" value="<?php echo set_value('email',$profile[0]->email); ?>" name="email">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="imgurl">アイコン</label><small>215x215以内</small>
                                        <input type="file" name="userfile" class="dropify" data-width="215" data-height="215" data-max-width="216" data-max-height="216" data-allowed-file-extensions="png jpg jpeg gif" data-default-file="<?php echo base_url('images/')?><?php echo $profile[0]->icon_url;?>">
                        
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="imgurl">電話番号</label><small>数字のみ</small>
                                        <input type="number" class="form-control" value="<?php echo set_value('mobile',$profile[0]->mobile); ?>" name="mobile">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="imgurl">*現パスワード   </label><small>半角英数字のみ</small>
                                        <input type="text" class="form-control" value="" name="password">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="imgurl">パスワード変更</label><small>変更する場合は記載　半角英数字のみ</small>
                                        <input type="text" class="form-control" value="" name="passwordNew">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="imgurl">パスワード変更 確認</label><small>変更する場合は記載　半角英数字のみ</small>
                                        <input type="text" class="form-control" value="" name="passwordRe">
                                    </div>
                                </div>
                            </div>

                           
                        </div>
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value=" 更 新 " />
                        </div>
                    </form>
                </div>
            </div>

        </div>    
    </section>
</div>

<script>
$(document).ready(function(){
	
	var addUserForm = $("#editPost");
	
	var validator = addUserForm.validate({
		
		rules:{
            title :{ required : true },
            url :{ required : true, remote : { url : baseURL + "/posts/recheckurl", type :"post", data:{reurl: function() { return $('#hidden-url').val(); }}} },
			password : { required : true },
			opendate : { required : true},
		},
		messages:{
            title :{ required : "入力必須です" },
            url :{ required : "入力必須です", remote : "使用済みURLです" },
			password : { required : "入力必須です" },
			opendate : { required : "入力必須です" },
		}
	});

    $('.dropify').dropify({
            // default file for the file input
            defaultFile: '',

            // max file size allowed
            maxFileSize: '2000K',

            // custom messages
            messages: {
                'default': '画像をドラックドロップまたはクリック',
                'replace': 'ドラックドロップまたはクリックで閉じて変更',
                'remove':  '取消',
                'error':   'エラー'
            },

			error: {
					'fileSize': 'ファイルサイズは {{ value }} まで.',
					'minWidth': 'The image width is too small ({{ value }}}px min).',
					'maxWidth': 'The image width is too big ({{ value }}}px max).',
					'minHeight': 'The image height is too small ({{ value }}}px min).',
					'maxHeight': 'The image height is too big ({{ value }}px max).',
					'imageFormat': 'The image format is not allowed444 ({{ value }} only).',
					'fileExtension': '拡張子は {{ value }} のみ許可.'
				}
    });
});
</script>
