<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Logout extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        
    }
        /**
     * Index Page for this controller.
     */
    public function index()
    {
        session_destroy();

        redirect(base_url('manager'));
    }
    
}