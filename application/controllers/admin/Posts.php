<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/AdminController.php';


class Posts extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        $this->load->model('admin_posts_model');
        $this->load->model('config_model');
        $this->load->helper('admin_helper');
    }
    

    public function index(){       
        
        $this->page();
    }
    

    function page()
    {
       
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->admin_posts_model->postsListingCount($searchText);

			$returns = $this->paginationCompress ( "/posts/page/", $count, 10 , 4);
            
            $data['postsRecords'] = $this->admin_posts_model->Listing($searchText, $returns["page"], $returns["segment"]);
            
            $this->load_options();
            $data['status'] = $this->data['status'];

            
            $this->global['pageTitle'] = 'ブログリスト';
            $this->foot['editor'] = 'off';
            
            $this->loadViews("admin/posts_list_view", $this->global, $data, $this->foot);
        
    }

    /**
     * new blob view
     */
    function addNew()
    {
 
            
            $this->load_options();
            $data['status'] = $this->data;
            
            $this->global['pageTitle'] = '新規ブログ';

            $this->loadViews("admin/posts_addnew_view", $this->global, $data, '');
       
    }


    
    /**
     * new blog post
     */
    function addNewPost()
    {

            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('title','タイトル','trim|required');
            $this->form_validation->set_rules('url','表示用URL','trim|required|is_unique[tbl_posts.url]');
            $this->form_validation->set_rules('content','コンテンツ','required');
            $this->form_validation->set_rules('status','ステータス','trim|required|numeric');
            $this->form_validation->set_rules('opendate','公開日','required');
            $this->form_validation->set_rules('opentime','公開時間','required');
            
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {



                if (!empty($_FILES['userfile']['name'])){
                    //$this->do_img_upload('images'); 
                    #$filename[$i] = $_FILES['userfile']['name'][$i];

                    $_FILES['userfile']['name'];
                    $_FILES['userfile']['type'];
                    $_FILES['userfile']['tmp_name'];
                    $_FILES['userfile']['error'];
                    $_FILES['userfile']['size'];

                        $this->load->library('upload', setuploadprotocol());
                        //$this->upload->initialize(setuploadprotocol());
                        
                        $return = $this->upload->do_upload();
                        

                    if (!$return)
                        {
                            //エラー                          
                            $this->file_errors = $this->upload->display_errors();

                        }else{
                            //成功

                            $data_one = array('upload_data' => $this->upload->data());
                            $filename = $data_one['upload_data']['file_name'];
                            
                        }


                }else{
                    $filename = '';
                }
               
                $title = $this->security->xss_clean($this->input->post('title'));
                $url = urlencode($this->security->xss_clean($this->input->post('url')));
                $content = html_escape($this->input->post('content'));
                $status = $this->input->post('status');
                $opendate = $this->security->xss_clean($this->input->post('opendate')).' '.$this->security->xss_clean($this->input->post('opentime'));
                
                $userInfo = array(
                    'title'=>$title,
                    'url'=>$url,
                    'img_url' => $filename,
                    'contents'=>$content,                    
                    'status'=>$status,
                    'opendate'=>$opendate
                    );
                $result = $this->admin_posts_model->addNewPost($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', '新規作成に成功しました'.$this->file_errors);
                }
                else
                {
                    $this->session->set_flashdata('error', '新規作成に失敗しました'.$this->file_errors);
                }
                
                redirect($this->adminurl.'/posts/editOld/'.$result);
            }
      
    }

    
    /**
     * Edit　view
     * 
     */
    function editOld($blogId = NULL)
    {

            if($blogId == null)
            {
                show_404();
            }
            $this->load_options();

            
            $data['status_list'] = $this->data;
            $data['postInfo'] = $this->admin_posts_model->getPostInfo($blogId);
            
            $this->global['pageTitle'] = 'ブログ編集';           
            $this->loadViews("admin/posts_edit_old", $this->global, $data, '');
        
    }
    
    
    /**
     * Edit update
     */
    function editPost()
    {

            $this->load->library('form_validation');
            
            $blogid = $this->input->post('id');
            
            $this->form_validation->set_rules('title','タイトル','trim|required');
            $this->form_validation->set_rules('hidden-url','元URL','trim|required');
            $this->form_validation->set_rules('url','表示用URL','trim|required|callback_reurlcheck');
            $this->form_validation->set_rules('content','コンテンツ','required');            
            $this->form_validation->set_rules('status','ステータス','trim|required|numeric');
            $this->form_validation->set_rules('opendata','公開日時','required');
            $this->form_validation->set_rules('opentime','公開時間','required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($blogid);
            }
            else
            {
                $title = $this->security->xss_clean($this->input->post('title'));
                $url = urlencode($this->security->xss_clean($this->input->post('url')));
                $img_url = urlencode($this->security->xss_clean($this->input->post('img_url')));
                $content = html_escape($this->input->post('content'));                
                $status = $this->input->post('status');
                $opendate = $this->security->xss_clean($this->input->post('opendata')).' '.$this->security->xss_clean($this->input->post('opentime'));
                
                if (!empty($_FILES['userfile']['name'])){
                    //$this->do_img_upload('images'); 
                    #$filename[$i] = $_FILES['userfile']['name'][$i];

                    $_FILES['userfile']['name'];
                    $_FILES['userfile']['type'];
                    $_FILES['userfile']['tmp_name'];
                    $_FILES['userfile']['error'];
                    $_FILES['userfile']['size'];

                        $this->load->library('upload', setuploadprotocol());
                        //$this->upload->initialize(setuploadprotocol());
                        
                        $return = $this->upload->do_upload();
                        

                    if (!$return)
                        {
                            //エラー                          
                            $this->file_errors = $this->upload->display_errors();

                        }else{
                            //成功

                            $data_one = array('upload_data' => $this->upload->data());
                            $filename = $data_one['upload_data']['file_name'];

                            $imgarray = array(
                                'img_url'=>$filename
                            );
                            
                        }


                }else{
                    $imgarray = array();
                }


                $insertdata = array(
                    'title'=>$title,
                    'url'=>$url, 
                    'contents'=>$content,                    
                    'status'=>$status,
                    'opendate'=>$opendate
                    );
                $insertdata = $imgarray + $insertdata;
                
                $result = $this->admin_posts_model->editPost($insertdata, $blogid);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', '更新しました');
                }
                else
                {
                    $this->session->set_flashdata('error', '更新に失敗しました');
                }
                
                redirect($this->adminurl.'/posts/editOld/'.$blogid.'');
            }
        
    }

    /*
    *   Ajax URL　チェック
    */
    function checkurl()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('url','URL','trim|required');
        
        if($this->form_validation->run() == FALSE)
        {
            echo(json_encode(array('status'=>FALSE)));
        }
        else
        {
            $url = $this->input->post('url');
            $return = $this->admin_posts_model->checkUrlExists($url);

            if(!empty($return)){
                echo 'false';
            }else{
                echo 'true';
            }


        }
    }
    /*
    *   Ajax Editing URL check
    */
    function recheckurl()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('url','URL','trim|required');
        $this->form_validation->set_rules('reurl','URL','trim|required');
        
        if($this->form_validation->run() == FALSE)
        {
            echo(json_encode(array('status'=>FALSE)));
        }
        else
        {
            $url = $this->input->post('url');
            $reurl = $this->input->post('reurl');
            $return = $this->admin_posts_model->recheckUrlExists($url,$reurl);

            if(!empty($return)){
                echo 'false';
            }else{
                echo 'true';
            }


        }
    }
    
    /*
    *   Form Editing URL check
    */

    function reurlcheck($url)
    {

        if(!empty($url)){

            
            $reurl = $this->input->post('hidden-url');
            $result = $this->admin_posts_model->recheckUrlExists($url,$reurl);

            if(empty($result)){
                return true;
            }
            else {
                $this->form_validation->set_message('reurlcheck', '使用済URLです');
                return FALSE;
            }
    
        }

    }

    /*
    *   Delate update
    *
    */
    
    function deletePost()
    {

        $id = $this->input->post('id');
        $intData = array('isDeleted'=>3);            
        $result = $this->admin_posts_model->deletePost($id, $intData);            
        if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
        else { echo(json_encode(array('status'=>FALSE))); }
        
    }



    

}

?>