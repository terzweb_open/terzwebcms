<?php

?>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b><?php echo $this->config->item('site_title'); ?></b> Admin System | Version 1.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="<?php echo base_url(); ?>"><?php echo $this->config->item('site_title'); ?></a>.</strong> All rights reserved.
    </footer>
    
    <!-- jQuery UI 1.11.2 -->
    <!-- <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script> -->
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/tabledit/jquery.tabledit.min.js"></script>

<!-- FastClick -->
	<!--  dropify Plugin    -->	
  <script src="<?php echo base_url(); ?>assets/plugins/dropify/js/dropify.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/validation.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
   <?php
  
      if(isset($editor) !='off')
      {
   ?>
    <!-- CK Editor -->
    <script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
 

 <script>
  $(function (){
    CKEDITOR.replace("editor1");
    CKEDITOR.config.height = "500px"; //高さ
    CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.filebrowserImageBrowseUrl = "<?php echo base_url(); ?>assets/plugins/kcfinder/browse.php?type=images";
    CKEDITOR.config.filebrowserImageUploadUrl = "<?php echo base_url(); ?>assets/plugins/kcfinder/upload.php?type=images";
    CKEDITOR.config.filebrowserBrowseUrl = "<?php echo base_url(); ?>assets/plugins/kcfinder/browse.php?type=files";
    CKEDITOR.config.filebrowserUploadUrl = "<?php echo base_url(); ?>assets/plugins/kcfinder/upload.php?type=files";
    CKEDITOR.config.contentsCss = "<?php echo base_url(); ?>assets/<?php echo $this->thistememe?>/css/myadmin.css"    
    //bootstrap WYSIHTML5 - text editor
  });
</script>
<?php
      }
?>

<script type="text/javascript">
    var windowURL = window.location.href;
    pageURL = windowURL.substring(0, windowURL.lastIndexOf('/'));
    console.log(windowURL);
    console.log(pageURL);
    var x= $('a[href="'+pageURL+'"]');
        x.addClass('active');
        x.parent().addClass('active');
    var y= $('a[href="'+windowURL+'"]');
        y.addClass('active');
        y.parent().addClass('active');
</script>
  </body>
</html>