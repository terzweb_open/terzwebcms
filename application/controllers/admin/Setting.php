<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/AdminController.php';


class Setting extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        $this->load->model('admin_config_model');

    }


    public function index()
    {  
        $this->list();
    }

    function list()
    {

           
            $this->load_options();
            $data = $this->data;
 
            
            $data['pageRecords'] = $this->admin_config_model->userListing('','');
            

            $this->global['pageTitle'] = '設定';
            $this->foot['editor'] = 'off';

            $this->loadViews("admin/settei", $this->global, $data, $this->foot);
        
    }

    function addNew()
    {

            $this->global['pageTitle'] = '設定追加';
            $this->foot['editor'] = 'off';

            $this->loadViews("admin/setteiNew", $this->global, NULL, $this->foot);
      
    }

    function adding()
    {

            $this->load->library('form_validation');
            
            //$this->form_validation->set_rules('id','configid','trim|required|numeric');
            $this->form_validation->set_rules('configname','configname','trim|required|is_unique[tbl_config.configname]',array('is_unique'=>'使用済みconfignameです'));
            $this->form_validation->set_rules('value','value','trim');
         
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {

                
                $dataInfo = array(
                    'configname'=> $this->security->xss_clean($this->input->post('configname')),
                    'value'=> $this->security->xss_clean($this->input->post('value'))
                    
                    );
                

                $result = $this->admin_config_model->addNewData($dataInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', '新規作成しました');
                }
                else
                {
                    $this->session->set_flashdata('error', '登録エラーでした');
                }
                
                redirect($this->adminurl.'/setting');
            }
        
    }



    function editupdate()
    {

            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('id','configid','trim|required|numeric');
            $this->form_validation->set_rules('value','value','trim');
            $this->form_validation->set_rules('action','action','trim|required');
         
            
            
            if($this->form_validation->run() == FALSE)
            {
                echo(json_encode(array('status'=>FALSE)));
                
            }
            else
            {
                
                $action = $this->security->xss_clean($this->input->post('action'));
                $configid = $this->security->xss_clean($this->input->post('id'));

                switch ($action) {
                    case 'edit':
                        # code...
                        
                        $value = $this->security->xss_clean($this->input->post('value'));
                        
                        $dataInfo = array(
                            'value'=> $value                   
                            );
                        
        
                        $result = $this->admin_config_model->updateData($dataInfo,$configid);
                        
                        
                        if($result == true)
                        {
                            $retrenallay = array(
                                'id' => $configid,
                                'value'=> $value
                            );
        
                            echo json_encode($retrenallay);
                        }
                        else
                        {
                            echo(json_encode(array('status'=>FALSE)));
                        }
                        
                        break;
                    
                    case 'delete':
                        # code...
                        $result = $this->admin_config_model->deleteData($configid);
                        if ($result > 0) {
                                $retrenallay = array(
                                    'id' => $configid,
                                    'value'=> ''
                                );
        
                            echo json_encode($retrenallay); 
                            }
                        else {
                            $retrenallay = array(
                                'id' => $configid,
                                'value'=> ''
                            );
        
                            echo json_encode($retrenallay); 
                            }


                    break;
                }





                
            }
        
    }




}