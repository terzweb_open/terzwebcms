-- --------------------------------------------------------
-- ホスト:                          localhost
-- サーバーのバージョン:                   10.4.6-MariaDB - mariadb.org binary distribution
-- サーバー OS:                      Win64
-- HeidiSQL バージョン:               11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

--  テーブル ggpjtipo_root_cmsterzwebcom.tbl_config の構造をダンプしています
CREATE TABLE IF NOT EXISTS `tbl_config` (
  `configid` int(11) NOT NULL AUTO_INCREMENT,
  `configname` varchar(50) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`configid`),
  KEY `configname` (`configname`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- テーブル ggpjtipo_root_cmsterzwebcom.tbl_config: ~6 rows (約) のデータをダンプしています
/*!40000 ALTER TABLE `tbl_config` DISABLE KEYS */;
INSERT INTO `tbl_config` (`configid`, `configname`, `value`) VALUES
	(1, 'theme', 'templete');
INSERT INTO `tbl_config` (`configid`, `configname`, `value`) VALUES
	(2, 'site_title', 'terzweb');
INSERT INTO `tbl_config` (`configid`, `configname`, `value`) VALUES
	(3, 'meta_keyword', 'WEBプロダクト');
INSERT INTO `tbl_config` (`configid`, `configname`, `value`) VALUES
	(7, 'top_blog_count', '5');
INSERT INTO `tbl_config` (`configid`, `configname`, `value`) VALUES
	(8, 'top_news_count', '4');
INSERT INTO `tbl_config` (`configid`, `configname`, `value`) VALUES
	(9, 'list_blog_count', '6');
/*!40000 ALTER TABLE `tbl_config` ENABLE KEYS */;

--  テーブル ggpjtipo_root_cmsterzwebcom.tbl_home の構造をダンプしています
CREATE TABLE IF NOT EXISTS `tbl_home` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contents` text NOT NULL,
  `lastupdate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- テーブル ggpjtipo_root_cmsterzwebcom.tbl_home: ~0 rows (約) のデータをダンプしています
/*!40000 ALTER TABLE `tbl_home` DISABLE KEYS */;
INSERT INTO `tbl_home` (`id`, `contents`, `lastupdate`) VALUES
	(1, '&lt;p&gt;&lt;a href=&quot;https://github.com/bcit-ci/CodeIgniter/wiki/Category%3ALibraries%3A%3ASession&quot;&gt;Category:Libraries::Session&lt;/a&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;うるとらまん&lt;/p&gt;\r\n\r\n&lt;h3&gt;Benefits over CI_Session&lt;/h3&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;hardened against session fixation by cookie id TTL (time to live) - regenerates cookie id automatically every given amount of time (right now configured inside the class) - see Note about making it setable.&lt;/li&gt;\r\n	&lt;li&gt;you can use all available PHP session storage drivers (database, memcache, etc.)&lt;/li&gt;\r\n	&lt;li&gt;&amp;quot;flash&amp;quot; session attributes (see: &amp;quot;Flash&amp;quot; attributes)&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;h3&gt;Benefits over PHPsession&lt;/h3&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;compatible with CI_Session\r\n	&lt;ul&gt;\r\n		&lt;li&gt;the same way of use, just load the library, set_userdata(), userdata()&lt;/li&gt;\r\n		&lt;li&gt;easy to migrate existing apps to Native_session&lt;/li&gt;\r\n		&lt;li&gt;need docs - use the CI manual :)&lt;/li&gt;\r\n	&lt;/ul&gt;\r\n	&lt;/li&gt;\r\n	&lt;li&gt;better security (automatic and manual session id regeneration)&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', '2020-12-09 16:00:17');
/*!40000 ALTER TABLE `tbl_home` ENABLE KEYS */;

--  テーブル ggpjtipo_root_cmsterzwebcom.tbl_menu の構造をダンプしています
CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort` int(11) DEFAULT NULL,
  `value` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- テーブル ggpjtipo_root_cmsterzwebcom.tbl_menu: ~4 rows (約) のデータをダンプしています
/*!40000 ALTER TABLE `tbl_menu` DISABLE KEYS */;
INSERT INTO `tbl_menu` (`id`, `sort`, `value`, `url`, `status`, `isDeleted`) VALUES
	(1, 10, 'HOME', 'http://localhost/cms.terzweb.com/', 1, 0);
INSERT INTO `tbl_menu` (`id`, `sort`, `value`, `url`, `status`, `isDeleted`) VALUES
	(2, 20, 'BLOG', 'post', 1, 0);
INSERT INTO `tbl_menu` (`id`, `sort`, `value`, `url`, `status`, `isDeleted`) VALUES
	(3, 30, 'About', 'site', 1, 0);
INSERT INTO `tbl_menu` (`id`, `sort`, `value`, `url`, `status`, `isDeleted`) VALUES
	(4, 40, 'contacts', 'contact', 1, 0);
/*!40000 ALTER TABLE `tbl_menu` ENABLE KEYS */;

--  テーブル ggpjtipo_root_cmsterzwebcom.tbl_news の構造をダンプしています
CREATE TABLE IF NOT EXISTS `tbl_news` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `contents` text NOT NULL,
  `contents_sub` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1下書き　2公開',
  `opendate` datetime NOT NULL,
  `postdata` timestamp NOT NULL DEFAULT current_timestamp(),
  `auther` tinyint(4) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- テーブル ggpjtipo_root_cmsterzwebcom.tbl_news: ~5 rows (約) のデータをダンプしています
/*!40000 ALTER TABLE `tbl_news` DISABLE KEYS */;
INSERT INTO `tbl_news` (`id`, `title`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(1, '解説！！！', '解説しました', '', 1, '0000-00-00 00:00:00', '2020-12-03 16:20:20', 0, 3);
INSERT INTO `tbl_news` (`id`, `title`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(2, '【PHP】 phpのdate（日付関数）のまとめ', 'あかさたな&lt;br /&gt;\r\nｍはあ', '', 1, '2020-12-07 02:04:00', '2020-12-05 23:24:18', 0, 0);
INSERT INTO `tbl_news` (`id`, `title`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(3, '【AMP】 サイトをAMP化するぞ。しかしそもそもAMP化とは。', 'あんぷあんぷ&lt;br /&gt;\r\n&amp;nbsp;', '', 1, '2020-12-05 23:12:00', '2020-12-05 23:31:38', 0, 0);
INSERT INTO `tbl_news` (`id`, `title`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(4, '【PHP】 phpのdate（日付関数）のまとめ', 'かんすうめいを&lt;br /&gt;\r\n書く', '', 1, '2020-12-07 11:12:00', '2020-12-07 11:13:41', 0, 0);
INSERT INTO `tbl_news` (`id`, `title`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(5, '芸能人 Youtube登録者ランキングを作成したよ', '作成したねん&lt;br /&gt;\r\n&lt;br /&gt;\r\nどうやねん&lt;br /&gt;\r\n&lt;br /&gt;\r\n橋本・・・', '', 2, '2020-12-09 17:00:00', '2020-12-07 11:20:40', 0, 0);
/*!40000 ALTER TABLE `tbl_news` ENABLE KEYS */;

--  テーブル ggpjtipo_root_cmsterzwebcom.tbl_pages の構造をダンプしています
CREATE TABLE IF NOT EXISTS `tbl_pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `contents` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `opendate` datetime NOT NULL,
  `postdata` timestamp NOT NULL DEFAULT current_timestamp(),
  `auther` tinyint(4) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- テーブル ggpjtipo_root_cmsterzwebcom.tbl_pages: ~8 rows (約) のデータをダンプしています
/*!40000 ALTER TABLE `tbl_pages` DISABLE KEYS */;
INSERT INTO `tbl_pages` (`id`, `url`, `title`, `contents`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(1, 'site', 'ページタイトル', 'サイト&lt;br /&gt;\r\nサイト', 2, '2020-12-11 10:22:00', '2020-12-03 15:59:59', 0, 0);
INSERT INTO `tbl_pages` (`id`, `url`, `title`, `contents`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(2, 'えeaeat', 'であ', 'daea', 1, '2020-09-30 00:00:00', '2020-12-07 14:44:54', 0, 3);
INSERT INTO `tbl_pages` (`id`, `url`, `title`, `contents`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(3, 'あいしてる', '恐怖の初めてシリーズ????　～ラストパーマ in 竹山', '雑誌の取材と称してカンニング竹山さんを呼び出し????&lt;br /&gt;\r\n何の企画と誤解したのか&lt;br /&gt;\r\n「俺もう金ないっすよ」と恐怖に慄く竹山さん。789', 1, '2020-12-07 22:12:00', '2020-12-07 22:41:39', 0, 0);
INSERT INTO `tbl_pages` (`id`, `url`, `title`, `contents`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(4, 'sites', 'あらましのその', '&lt;p&gt;基本構文は下記になります&lt;/p&gt;\r\n\r\n&lt;pre&gt;\r\n&lt;code&gt;mb_substr( 対象文字列, 開始位置 [ ,文字数 [ ,エンコーディング]] );\r\n&lt;/code&gt;&lt;/pre&gt;\r\n\r\n&lt;p&gt;mb_substr()関数は、基本的にはsubstr()関数と使用方法は同じですが、マルチバイト文字列(ひらがなや漢字といった日本語の文字列)を切り出す(抽出・または抜き出し)ことができます。&lt;/p&gt;\r\n\r\n&lt;pre&gt;\r\n&lt;code&gt;echo mb_substr(&amp;#39;コードキャンプ&amp;#39;, 3);\r\n&lt;/code&gt;&lt;/pre&gt;\r\n\r\n&lt;p&gt;結果&lt;/p&gt;\r\n\r\n&lt;pre&gt;\r\n&lt;code&gt;&amp;quot;キャンプ&amp;quot;\r\n&lt;/code&gt;&lt;/pre&gt;\r\n\r\n&lt;p&gt;同様に、引数に負の数を&lt;/p&gt;\r\n', 1, '2020-12-07 23:12:00', '2020-12-07 23:31:27', 0, 0);
INSERT INTO `tbl_pages` (`id`, `url`, `title`, `contents`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(5, 'profile', '筆者プロフィール', '&lt;strong&gt;横山茂雄（よこやましげお）&lt;/strong&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;フリーエンジニアとして活動中。サーバーサイドからフロントまで時代の波に合わせてスキルを変化させてきました。&lt;/p&gt;\r\n\r\n&lt;p&gt;言語、フレームワーク、DB、現場、いずれも転々としながら、筋トレも欠かさない体育会系エンジニアです。TechAcademyジュニアのゲームアプリコースを担当しています。&lt;/p&gt;\r\n', 1, '2020-12-07 23:12:00', '2020-12-07 23:34:56', 0, 0);
INSERT INTO `tbl_pages` (`id`, `url`, `title`, `contents`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(6, 'profiles', '筆者プロフィール', '&lt;strong&gt;横山茂雄（よこやましげお）&lt;/strong&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;フリーエンジニアとして活動中。サーバーサイドからフロントまで時代の波に合わせてスキルを変化させてきました。&lt;/p&gt;\r\n\r\n&lt;p&gt;言語、フレームワーク、DB、現場、いずれも転々としながら、筋トレも欠かさない体育会系エンジニアです。TechAcademyジュニアのゲームアプリコースを担当しています。&lt;/p&gt;\r\n', 1, '2020-12-07 23:12:00', '2020-12-07 23:35:21', 0, 0);
INSERT INTO `tbl_pages` (`id`, `url`, `title`, `contents`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(7, 'profiless', '筆者プロフィール', '&lt;p&gt;ウェブアプリケーションを作る際に、指定のディレクトリ下のプログラムファイルを自ドメイン以外からアクセスできなくする必要が出てくることがあります。&lt;/p&gt;\r\n\r\n&lt;p&gt;ajaxで叩くプログラムファイルだったり、cronで指定の時間に叩くようなプログラムファイルなんかがそれにあたります。&lt;/p&gt;\r\n\r\n&lt;p&gt;例えばですが、cronで毎月1日に実行する集計プログラムがあるとします。&lt;/p&gt;\r\n\r\n&lt;p&gt;もしこのプログラムをユーザーが直接URLで叩いて集計を実行してしまったら大変困ったことになります。&lt;/p&gt;\r\n\r\n&lt;p&gt;自ドメインからファイルのアクセスを禁止する方法は、PHPでプログラム内に仕込む方法もありますが、指定のディレクトリに置いて.htaccessでそのディレクトリ化のファイルをすべて自ドメイン以外からアクセス禁止にしてしまうと管理しやすいかと思います。&lt;/p&gt;\r\n\r\n&lt;p&gt;以下がその際の.htaccessの記述方法です。&lt;/p&gt;\r\n', 2, '2020-12-07 23:12:00', '2020-12-07 23:35:31', 0, 0);
INSERT INTO `tbl_pages` (`id`, `url`, `title`, `contents`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(8, 'unname', 'Around40〜注文の多いオンナたち〜', '&lt;p&gt;TechAcademyでは、初心者でも最短4週間で、JavaScript・jQueryを使ったWebサービス公開を習得できる、&lt;a href=&quot;https://techacademy.jp/frontend-bootcamp?utm_source=tamagazine&amp;amp;utm_medium=referral&amp;amp;utm_content=19623&amp;amp;utm_campaign=post&quot; rel=&quot;noopener&quot; target=&quot;_blank&quot;&gt;オンラインブートキャンプ&lt;/a&gt;を開催しています。&lt;/p&gt;\r\n\r\n&lt;p&gt;また、現役エンジニアから学べる&lt;a href=&quot;https://techacademy.jp/lp-htmlcss-trial-s?utm_source=tamagazine&amp;amp;utm_medium=referral&amp;amp;utm_content=22328&amp;amp;utm_campaign=post&quot; rel=&quot;noopener&quot; target=&quot;_blank&quot;&gt;無料体験&lt;/a&gt;も実施しているので、ぜひ参加してみてください。&lt;/p&gt;\r\n', 2, '2020-12-07 23:12:00', '2020-12-07 23:36:22', 0, 0);
/*!40000 ALTER TABLE `tbl_pages` ENABLE KEYS */;

--  テーブル ggpjtipo_root_cmsterzwebcom.tbl_posts の構造をダンプしています
CREATE TABLE IF NOT EXISTS `tbl_posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `title` varchar(50) NOT NULL,
  `img_url` text NOT NULL,
  `contents` text NOT NULL,
  `contents_sub` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `opendate` datetime NOT NULL,
  `postdata` timestamp NOT NULL DEFAULT current_timestamp(),
  `auther` tinyint(4) NOT NULL,
  `isDeleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- テーブル ggpjtipo_root_cmsterzwebcom.tbl_posts: ~8 rows (約) のデータをダンプしています
/*!40000 ALTER TABLE `tbl_posts` DISABLE KEYS */;
INSERT INTO `tbl_posts` (`id`, `url`, `title`, `img_url`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(1, 'php-date-howto', '【PHP】 phpのdate（日付関数）のまとめ', '7c2231b9d4b9e0514212d7241c590763.jpg', 'tedea', '', 2, '2020-12-03 11:13:00', '2020-12-03 11:13:48', 0, 0);
INSERT INTO `tbl_posts` (`id`, `url`, `title`, `img_url`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(2, 'php-date-howto10', '株式会社スターホールディングス-', '840357b935d19cbc1555d5f9534920b8.JPG', '&lt;p&gt;の場合は、下記のように文字列の末尾から数えた文字数分の切り出しを行うことができます。&lt;/p&gt;\r\n\r\n&lt;pre&gt;\r\n&lt;code&gt;&amp;quot;camp&amp;quot;\r\n&lt;/code&gt;&lt;/pre&gt;\r\n\r\n&lt;p&gt;注意点として、substr()関数では、マルチバイト文字列(ひらがなや漢字といった日本語の文字列)を切り出す(抽出・または抜き出し)ことはできません。&lt;/p&gt;\r\n', '', 2, '2020-12-02 05:06:00', '2020-12-07 11:58:17', 0, 0);
INSERT INTO `tbl_posts` (`id`, `url`, `title`, `img_url`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(3, '1078245563', '【驚愕】女優の観月ありさに新疑惑...マヂかよこれ・・・・・', '', '&lt;h2 id=&quot;t_title&quot;&gt;【芸能】観月ありさ、布袋寅泰との並び写真で真実味を帯び始めた「身長逆サバ」疑惑！ [幻の右★]&lt;/h2&gt;\r\n&lt;br /&gt;\r\n1:&amp;nbsp;幻の右 ★&amp;nbsp;2020/12/05(土) 23:45:15.43&amp;nbsp;&lt;i&gt;ID:CAP&lt;/i&gt;_USER9&lt;br /&gt;\r\n　女優の観月ありさが11月21日、インスタグラムで布袋寅泰との並び写真を公開。これが大きな反響を呼んでいる。&lt;br /&gt;\r\n&lt;br /&gt;\r\n「観月には以前から身長の逆サバ疑惑が持ち上がっています。彼女は身長170センチとされていますが、実際にはもっとあるのではないかと疑われています。今回の写真で改めて指摘されました」（芸能ライター）&lt;br /&gt;\r\n&lt;br /&gt;\r\n　写真は以前に撮ったもので、2人が並んで立ち、布袋が観月の肩に手を回している。身長の差は布袋の頭で3分の1ほど。大きな差は感じられない。&lt;br /&gt;\r\n&lt;br /&gt;\r\n　布袋の身長は187センチ。公表されているデータによれば差は17センチになるが、写真ではそこまでの差は感じられない。&lt;br /&gt;\r\n&lt;br /&gt;\r\n「差は10センチ程度でしょうか。布袋がしゃんとしているの対して、観月は背を丸めているように見えます。普通に立てば身長差はもっと少なくなるのではないでしょうか。もし観月の身長が170センチよりあっても、隠すようなことではありません。むしろ誇らしいこと。正直に告白してもいいのではないかとの指摘もあります」（前出・芸能ライター）&lt;br /&gt;\r\n&lt;br /&gt;\r\n　足元は写っていないので、観月がかかとの高い靴をはいていないとも言い切れないが、服装は、そうした靴を履くようなドレスなどではない黒っぽいラフなズボンスタイルの服装だ。はたして、観月ありさが「本当の身長をカミングアウト」する日はいつやってくるだろうか。&lt;br /&gt;\r\n&lt;br /&gt;\r\n2020年12月5日 17:58&lt;a href=&quot;https://www.asagei.com/excerpt/164361&quot; target=&quot;_blank&quot;&gt;https://www.asagei.com/excerpt/164361&lt;/a&gt;&lt;br /&gt;\r\nインスタグラム&lt;br /&gt;\r\n&lt;a href=&quot;https://www.instagram.com/p/CH2i3HDnF4L/?igshid=1nfolkhhuxjeu&quot; target=&quot;_blank&quot;&gt;https://www.instagram.com/p/CH2i3HDnF4L/?igshid=1nfolkhhuxjeu&lt;/a&gt;', '', 2, '2020-12-07 14:12:00', '2020-12-07 14:27:04', 0, 0);
INSERT INTO `tbl_posts` (`id`, `url`, `title`, `img_url`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(4, '57468303', '日本政府、新コロナの影響もあり100兆円チャージ！まだまだリーマン・ショック時の2倍（52兆円）程度', '', '43:&amp;nbsp;白色矮星(東京都) [US]&amp;nbsp;2020/12/07(月) 08:35:19.57 ID:xO8RhOPn0&lt;br /&gt;\r\n&amp;gt;&amp;gt;1&lt;br /&gt;\r\nあほか！　日本はケチったから景気の戻りが遅いんだぜ？&lt;br /&gt;\r\nPMI値を調べてみろよ　ちょっとしか死んでないのにケチったせいで被害がでかい&lt;br /&gt;\r\n&lt;br /&gt;\r\n&lt;br /&gt;\r\n2:&amp;nbsp;エウロパ(光) [IT]&amp;nbsp;2020/12/07(月) 07:40:55.99 ID:eI0iCO8m0&lt;br /&gt;\r\n全部、日銀が引き受けて即日償還で借金ゼロ&lt;br /&gt;\r\n&lt;br /&gt;\r\n&lt;br /&gt;\r\n4:&amp;nbsp;赤色超巨星(茸) [NO]&amp;nbsp;2020/12/07(月) 07:44:16.22 ID:4k5LSknw0&lt;br /&gt;\r\nそれが国民の望みだろ&lt;br /&gt;\r\n10万とか補償受け取らなかった奴なんて居るの？&lt;br /&gt;\r\n&lt;br /&gt;\r\n&lt;br /&gt;\r\n5:&amp;nbsp;レア(栃木県) [JP]&amp;nbsp;2020/12/07(月) 07:49:43.34 ID:uAW0gE5P0&lt;br /&gt;\r\n足んなきゃ紙屑になるまで日銀に刷らせろ&lt;br /&gt;\r\n&lt;br /&gt;\r\n&lt;br /&gt;\r\n7:&amp;nbsp;トリトン(東京都) [US]&amp;nbsp;2020/12/07(月) 07:55:28.13 ID:zlU5ozjz0&lt;br /&gt;\r\n刷れば刷るほど金になる&lt;br /&gt;\r\n&amp;nbsp;', '', 2, '2020-12-07 14:12:00', '2020-12-07 14:29:17', 0, 0);
INSERT INTO `tbl_posts` (`id`, `url`, `title`, `img_url`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(5, 'StarHoldings', '【PHP】 phpのdate（日付関数）のまとめ', '', 'えーーーー&lt;br /&gt;\r\n&amp;nbsp;', '', 2, '2020-12-08 23:12:00', '2020-12-08 23:29:10', 0, 0);
INSERT INTO `tbl_posts` (`id`, `url`, `title`, `img_url`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(6, 'StarHolding', '【PHP】 phpのdate（日付関数）のまとめ', '', 'ンテンツ', '', 2, '2020-12-08 23:12:00', '2020-12-08 23:30:30', 0, 0);
INSERT INTO `tbl_posts` (`id`, `url`, `title`, `img_url`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(7, 'about_site', 'Around40〜注文の多いオンナたち〜', '460e2dff053060c8c1801d708f14d1af.jpg', 'aeaefa', '', 2, '2020-12-08 23:12:00', '2020-12-08 23:52:19', 0, 0);
INSERT INTO `tbl_posts` (`id`, `url`, `title`, `img_url`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(8, 'privacy_policys', '芸能人 Youtube登録者ランキングを作成したよ', '7c2231b9d4b9e0514212d7241c590763.jpg', 'adafae', '', 2, '2020-12-09 00:12:00', '2020-12-09 00:08:04', 0, 0);
INSERT INTO `tbl_posts` (`id`, `url`, `title`, `img_url`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(9, '896586', '【API】 Instagram Graph APIを使ってインスタグラムの画像をWebサイトにPHP', '4f747b2c40824fb6e8143d69dd342643.png', 'あｄふぁだ', '', 2, '2020-12-09 10:12:00', '2020-12-09 10:14:29', 0, 0);
INSERT INTO `tbl_posts` (`id`, `url`, `title`, `img_url`, `contents`, `contents_sub`, `status`, `opendate`, `postdata`, `auther`, `isDeleted`) VALUES
	(10, '938k2', 'ただかｋだけああ', '840357b935d19cbc1555d5f9534920b8.JPG', 'adfaeaｓｄががｆｄ&lt;br /&gt;\r\nあえｆｇぁｊｄが&lt;br /&gt;\r\n、ぁｓｄｆｌｋｇじゃえｒｔ', '', 2, '2020-12-09 10:12:00', '2020-12-09 10:16:07', 0, 0);
/*!40000 ALTER TABLE `tbl_posts` ENABLE KEYS */;

--  テーブル ggpjtipo_root_cmsterzwebcom.tbl_roles の構造をダンプしています
CREATE TABLE IF NOT EXISTS `tbl_roles` (
  `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text',
  PRIMARY KEY (`roleId`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- テーブル ggpjtipo_root_cmsterzwebcom.tbl_roles: 4 rows のデータをダンプしています
/*!40000 ALTER TABLE `tbl_roles` DISABLE KEYS */;
INSERT INTO `tbl_roles` (`roleId`, `role`) VALUES
	(1, 'Admin');
INSERT INTO `tbl_roles` (`roleId`, `role`) VALUES
	(2, 'Manager');
INSERT INTO `tbl_roles` (`roleId`, `role`) VALUES
	(3, 'Employee');
INSERT INTO `tbl_roles` (`roleId`, `role`) VALUES
	(4, 'Member');
/*!40000 ALTER TABLE `tbl_roles` ENABLE KEYS */;

--  テーブル ggpjtipo_root_cmsterzwebcom.tbl_users の構造をダンプしています
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `icon_url` varchar(512) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `roleId` tinyint(4) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- テーブル ggpjtipo_root_cmsterzwebcom.tbl_users: ~0 rows (約) のデータをダンプしています
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` (`userId`, `email`, `password`, `name`, `icon_url`, `mobile`, `roleId`, `isDeleted`, `createdBy`, `createdDtm`, `updatedBy`, `updatedDtm`) VALUES
	(1, 'terzweb@gmail.com', '$2y$10$iO1cMJJ6PfW3pf8f4qoGq.PVDaph0QewFrVcv7viaceLSIAMVzQPe', '湯澤照人', '6297b3aa291f7bb3f59397b8794b147e.png', '09017970852', 1, 0, 0, '2015-07-01 18:56:49', 1, '2020-12-09 01:47:36');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
